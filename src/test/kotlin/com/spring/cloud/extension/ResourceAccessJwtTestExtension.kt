package com.spring.cloud.extension

import assertk.Assert
import assertk.assertions.isEqualTo
import assertk.assertions.prop
import com.spring.cloud.oauth2.resource.server.jwt.model.ResourceAccessJwt
import java.time.Instant

fun Assert<ResourceAccessJwt>.hasTokenEqualTo(token: String): Assert<ResourceAccessJwt> {
    prop("tokenValue") { it.tokenValue }.isEqualTo(token)
    return this
}

fun Assert<ResourceAccessJwt>.hasIssuedAtEqualTo(issuedAt: Instant): Assert<ResourceAccessJwt> {
    prop("issuedAt") { it.issuedAt }.isEqualTo(issuedAt)
    return this
}

fun Assert<ResourceAccessJwt>.hasExpiresAtEqualTo(expiresAt: Instant): Assert<ResourceAccessJwt> {
    prop("expiresAt") { it.expiresAt }.isEqualTo(expiresAt)
    return this
}

fun Assert<ResourceAccessJwt>.hasHeadersEqualTo(headers: Map<String, Any>): Assert<ResourceAccessJwt> {
    prop("headers") { it.headers }.isEqualTo(headers)
    return this
}

fun Assert<ResourceAccessJwt>.hasClaimsEqualTo(claims: Map<String, Any>): Assert<ResourceAccessJwt> {
    prop("claims") { it.claims }.isEqualTo(claims)
    return this
}
