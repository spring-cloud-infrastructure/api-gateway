package com.spring.cloud.extension

import assertk.Assert
import assertk.assertions.isEqualTo
import assertk.assertions.isNotNull
import assertk.assertions.prop
import com.spring.cloud.oauth2.resource.server.jwt.model.JwtGrantedAuthority
import com.spring.cloud.oauth2.resource.server.jwt.model.ResourceAccessJwt
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken

fun Assert<JwtAuthenticationToken>.hasAuthoritiesEqualTo(grantedAuthorities: Set<JwtGrantedAuthority>): Assert<JwtAuthenticationToken> {
    prop("authorities") { it.authorities }.isNotNull()
    val expectedAuthorities = grantedAuthorities.map(GrantedAuthority::getAuthority)
    prop("authorities") { it.authorities.map(GrantedAuthority::getAuthority) }.isEqualTo(expectedAuthorities)
    return this
}

fun Assert<JwtAuthenticationToken>.assertToken() = prop("token") { it.token as ResourceAccessJwt }
