package com.spring.cloud.extension

import assertk.Assert
import assertk.assertions.isEqualTo
import assertk.assertions.prop
import com.spring.cloud.oauth2.resource.server.jwt.provider.model.TokenExchangeRequest

fun Assert<TokenExchangeRequest>.hasSourceClientIdEqualTo(sourceClientId: String): Assert<TokenExchangeRequest> {
    prop("sourceClientId") { it.sourceClientId }.isEqualTo(sourceClientId)
    return this
}

fun Assert<TokenExchangeRequest>.hasSourceTokenEqualTo(sourceToken: String): Assert<TokenExchangeRequest> {
    prop("sourceToken") { it.sourceToken }.isEqualTo(sourceToken)
    return this
}

fun Assert<TokenExchangeRequest>.hasTargetClientIdEqualTo(targetClientId: String): Assert<TokenExchangeRequest> {
    prop("targetClientId") { it.targetClientId }.isEqualTo(targetClientId)
    return this
}
