package com.spring.cloud.extension

import assertk.Assert
import assertk.assertions.hasSize
import assertk.assertions.isEqualTo
import assertk.assertions.isFalse
import assertk.assertions.isTrue
import assertk.assertions.prop
import org.springframework.security.oauth2.core.OAuth2Error
import org.springframework.security.oauth2.core.OAuth2TokenValidatorResult

fun Assert<OAuth2TokenValidatorResult>.hasNoErrors(): Assert<OAuth2TokenValidatorResult> {
    prop("hasErrors") { it.hasErrors() }.isFalse()
    return this
}

fun Assert<OAuth2TokenValidatorResult>.hasAnyErrors(): Assert<OAuth2TokenValidatorResult> {
    prop("hasErrors") { it.hasErrors() }.isTrue()
    return this
}

fun Assert<OAuth2TokenValidatorResult>.hasErrorsEqualTo(errors: Collection<OAuth2Error>): Assert<OAuth2TokenValidatorResult> {
    prop("errors") { it.errors }.isEqualTo(errors)
    return this
}

fun Assert<OAuth2TokenValidatorResult>.hasErrorsSize(errorsSize: Int): Assert<OAuth2TokenValidatorResult> {
    prop("errors") { it.errors }.hasSize(errorsSize)
    return this
}

fun Assert<OAuth2TokenValidatorResult>.assertFirstError() = prop("errors") { it.errors.first() }
