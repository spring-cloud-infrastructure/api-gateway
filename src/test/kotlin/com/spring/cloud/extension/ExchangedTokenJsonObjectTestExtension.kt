package com.spring.cloud.extension

import assertk.Assert
import assertk.assertions.isEqualTo
import assertk.assertions.prop
import net.minidev.json.JSONObject

fun Assert<JSONObject>.hasAccessTokenEqualTo(accessToken: String): Assert<JSONObject> {
    prop("access_token") { it["access_token"] }.isEqualTo(accessToken)
    return this
}

fun Assert<JSONObject>.hasExpiresInEqualTo(expiresIn: Int): Assert<JSONObject> {
    prop("expires_in") { it["expires_in"] }.isEqualTo(expiresIn)
    return this
}

fun Assert<JSONObject>.hasRefreshTokenEqualTo(refreshToken: String): Assert<JSONObject> {
    prop("refresh_token") { it["refresh_token"] }.isEqualTo(refreshToken)
    return this
}

fun Assert<JSONObject>.hasRefreshExpiresInEqualTo(refreshExpiresIn: Int): Assert<JSONObject> {
    prop("refresh_expires_in") { it["refresh_expires_in"] }.isEqualTo(refreshExpiresIn)
    return this
}

fun Assert<JSONObject>.hasTokenTypeEqualTo(tokenType: String): Assert<JSONObject> {
    prop("token_type") { it["token_type"] }.isEqualTo(tokenType)
    return this
}

fun Assert<JSONObject>.hasNotBeforePolicyEqualTo(notBeforePolicy: Int): Assert<JSONObject> {
    prop("not-before-policy") { it["not-before-policy"] }.isEqualTo(notBeforePolicy)
    return this
}

fun Assert<JSONObject>.hasSessionStateEqualTo(sessionState: String): Assert<JSONObject> {
    prop("session_state") { it["session_state"] }.isEqualTo(sessionState)
    return this
}

fun Assert<JSONObject>.hasScopeEqualTo(scope: String): Assert<JSONObject> {
    prop("scope") { it["scope"] }.isEqualTo(scope)
    return this
}
