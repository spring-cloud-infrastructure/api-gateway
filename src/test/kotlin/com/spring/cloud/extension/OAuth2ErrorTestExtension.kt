package com.spring.cloud.extension

import assertk.Assert
import assertk.assertions.isEqualTo
import assertk.assertions.prop
import org.springframework.security.oauth2.core.OAuth2Error

fun Assert<OAuth2Error>.hasErrorCodeEqualTo(errorCode: String): Assert<OAuth2Error> {
    prop("errorCode") { it.errorCode }.isEqualTo(errorCode)
    return this
}

fun Assert<OAuth2Error>.hasDescriptionEqualTo(description: String): Assert<OAuth2Error> {
    prop("description") { it.description }.isEqualTo(description)
    return this
}

fun Assert<OAuth2Error>.hasUriEqualTo(uri: String): Assert<OAuth2Error> {
    prop("uri") { it.uri }.isEqualTo(uri)
    return this
}
