package com.spring.cloud.config.cache

import assertk.assertThat
import assertk.assertions.isNotNull
import com.hazelcast.spring.cache.HazelcastCacheManager
import com.spring.cloud.config.oauth2.resource.server.jwt.exchange.TokenExchangeProperties
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
internal class HazelcastCacheConfigurationIntegrationTest(
    @Autowired private val tokenExchangeProperties: TokenExchangeProperties,
    @Autowired private val underTest: HazelcastCacheManager,
) {

    @Test
    fun `given hazelcast cache is configured when token exchange cache is requested then cache is returned`() {
        assertThat(underTest.getCache(tokenExchangeProperties.cache.name)).isNotNull()
    }
}
