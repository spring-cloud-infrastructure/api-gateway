package com.spring.cloud.gateway.filter

import assertk.assertThat
import assertk.assertions.isEqualTo
import io.mockk.Called
import io.mockk.every
import io.mockk.mockk
import io.mockk.spyk
import io.mockk.verify
import org.junit.jupiter.api.Test
import org.springframework.cloud.gateway.filter.GatewayFilterChain
import org.springframework.http.HttpHeaders.AUTHORIZATION
import org.springframework.mock.http.server.reactive.MockServerHttpRequest
import org.springframework.mock.web.server.MockServerWebExchange
import org.springframework.security.oauth2.jwt.Jwt
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken
import reactor.core.publisher.Mono
import reactor.kotlin.core.publisher.toMono

internal class TokenExchangeGatewayFilterFactoryTest {

    private val chain = mockk<GatewayFilterChain>()
    private val jwtAuthenticationTokenPrincipal = mockk<JwtAuthenticationToken>()
    private val token = mockk<Jwt>()

    private val underTest = TokenExchangeGatewayFilterFactory()

    companion object {
        private const val JWT_ACCESS_TOKEN =
            "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJKeUVlaGdoenRIZk5wQzhHRG9oY1V0bGlTREJhOUtlaHRPalI2eDRGZmxFIn0.eyJleHAiOjE2MTUwNjk4NzMsImlzcyI6Imh0dHBzOi8vMTAuMC4wLjE6ODAvYXV0aC9yZWFsbXMvZGV2Iiwic3ViIjoiYjIyODc2ZjUtOTdiYS00MDRhLWJjMTctNjQwMjk0N2NkZGFiIiwidHlwIjoiQmVhcmVyIiwiYXpwIjoidGVzdC1jbGllbnQiLCJzZXNzaW9uX3N0YXRlIjoiZjA0ZTA3YzUtZmQ2My00YmZjLWFlMGItZWM5MDU5MmU2ZDY1In0.PPV5b-RQyqzdZvjKVHPkgPBIwLBqS-09YMg-wKZA1Lzo1givqboEE95U8qNLR9AM69wJsIHUem7vul6nBwPWRO5Cb7_Jo68G0gGs2XrlhzgTFVkbcYlJhkdW_IGRxGDTlM7dhUVezRAY7skTcX6EHVw9EM1FMCkpqwHUb-JHUb2ftUhoVhUzg7bWzkaUE_Uj_a9AlPFg8Paw1L1SL2lg7BD6gr51EWSdkhLafCt0z_PY8-Cs4wWRfc52m72TIZgwwLeSnKsvY6hDr5uhN0RF3k65ylHp-Io0UUhTTC3FHGLXAiHCz8AxBXQmoT7KknBPnupqoMG0Aa8MnmdGSAiuKQ"
    }

    @Test
    fun `given exchange holds JWT authentication token when filter is getting applied then authorization header is overridden`() {
        val exchange = spyk(MockServerWebExchange.from(MockServerHttpRequest.get("http://hello.com/test").build()))
        every { exchange.getPrincipal<JwtAuthenticationToken>() } returns jwtAuthenticationTokenPrincipal.toMono()
        every { jwtAuthenticationTokenPrincipal.token } returns token
        every { token.tokenValue } returns JWT_ACCESS_TOKEN
        every { chain.filter(any()) } returns Mono.empty()

        underTest.apply(TokenExchangeGatewayFilterConfig()).filter(exchange, chain).block()

        verify {
            exchange.getPrincipal<JwtAuthenticationToken>()
            jwtAuthenticationTokenPrincipal.token
            token.tokenValue
            chain.filter(withArg {
                assertThat(it.request.headers[AUTHORIZATION]).isEqualTo(listOf("Bearer $JWT_ACCESS_TOKEN"))
            })
        }
    }

    @Test
    fun `given exchange does not hold JWT authentication token when filter is getting applied then authorization header is not overridden`() {
        val exchange = spyk(MockServerWebExchange.from(MockServerHttpRequest.get("http://hello.com/test").build()))
        every { chain.filter(any()) } returns Mono.empty()

        underTest.apply(TokenExchangeGatewayFilterConfig()).filter(exchange, chain).block()

        verify {
            chain.filter(exchange)
            exchange.mutate() wasNot Called
        }
    }
}
