package com.spring.cloud

import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.test.context.TestConfiguration
import org.springframework.cloud.client.DefaultServiceInstance
import org.springframework.cloud.client.ServiceInstance
import org.springframework.cloud.loadbalancer.core.RoundRobinLoadBalancer
import org.springframework.cloud.loadbalancer.core.ServiceInstanceListSupplier
import org.springframework.cloud.loadbalancer.support.SimpleObjectProvider
import org.springframework.context.annotation.Bean
import reactor.core.publisher.Flux
import reactor.kotlin.core.publisher.toFlux
import reactor.kotlin.core.publisher.toMono

@TestConfiguration
internal class TestLoadBalancerConfiguration {

    companion object {
        private const val AUTHORIZATION_SERVER_ID = "authorization-server"
        private const val WIREMOCK_SERVER_HOST = "localhost"
    }

    @Bean
    fun authorizationServerLoadBalancer(
        @Value("\${wiremock.server.port}") wiremockServerPort: Int, // TODO: migrate to WireMockServer, once wem use Spring 2.4.x
    ): RoundRobinLoadBalancer {
        val serviceInstanceListSupplier = AuthorizationServerServiceInstanceListSupplier(wiremockServerPort)
        return RoundRobinLoadBalancer(SimpleObjectProvider(serviceInstanceListSupplier), AUTHORIZATION_SERVER_ID)
    }

    internal inner class AuthorizationServerServiceInstanceListSupplier(
        private val wiremockServerPort: Int,
    ) : ServiceInstanceListSupplier {

        override fun get(): Flux<MutableList<ServiceInstance>> {
            val serviceInstance: ServiceInstance = DefaultServiceInstance(
                AUTHORIZATION_SERVER_ID,
                AUTHORIZATION_SERVER_ID,
                WIREMOCK_SERVER_HOST,
                wiremockServerPort,
                false
            )
            return mutableListOf(serviceInstance).toMono().toFlux()
        }

        override fun getServiceId(): String {
            return AUTHORIZATION_SERVER_ID
        }
    }
}
