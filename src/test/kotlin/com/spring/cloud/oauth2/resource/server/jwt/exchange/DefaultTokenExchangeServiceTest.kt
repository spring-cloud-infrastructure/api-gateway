package com.spring.cloud.oauth2.resource.server.jwt.exchange

import assertk.assertThat
import assertk.assertions.isFailure
import assertk.assertions.isInstanceOf
import assertk.assertions.isNotNull
import com.spring.cloud.extension.hasClaimsEqualTo
import com.spring.cloud.extension.hasExpiresAtEqualTo
import com.spring.cloud.extension.hasHeadersEqualTo
import com.spring.cloud.extension.hasIssuedAtEqualTo
import com.spring.cloud.extension.hasSourceClientIdEqualTo
import com.spring.cloud.extension.hasSourceTokenEqualTo
import com.spring.cloud.extension.hasTargetClientIdEqualTo
import com.spring.cloud.extension.hasTokenEqualTo
import com.spring.cloud.oauth2.resource.server.jwt.exchange.validator.TokenToExchangeValidator
import com.spring.cloud.oauth2.resource.server.jwt.model.ResourceAccessJwt
import com.spring.cloud.oauth2.resource.server.jwt.provider.AuthorizationServerApiClient
import com.spring.cloud.problem.ExchangedTokenValidationProblem
import com.spring.cloud.problem.TokenToExchangeValidationProblem
import com.spring.cloud.problem.UnparseableExchangedTokenProblem
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import net.minidev.json.JSONObject
import org.junit.jupiter.api.Test
import org.springframework.security.oauth2.client.registration.ClientRegistration
import org.springframework.security.oauth2.core.OAuth2Error
import org.springframework.security.oauth2.core.OAuth2ErrorCodes.INVALID_REQUEST
import org.springframework.security.oauth2.core.OAuth2TokenValidatorResult.failure
import org.springframework.security.oauth2.core.OAuth2TokenValidatorResult.success
import org.springframework.security.oauth2.jwt.Jwt
import org.springframework.security.oauth2.jwt.JwtClaimNames
import org.springframework.security.oauth2.jwt.ReactiveJwtDecoder
import reactor.kotlin.core.publisher.toMono
import java.time.LocalDateTime
import java.time.ZoneOffset

@ExperimentalStdlibApi
internal class DefaultTokenExchangeServiceTest {

    private val tokenToExchangeValidator = mockk<TokenToExchangeValidator>()
    private val authorizationServerApiClient = mockk<AuthorizationServerApiClient>()
    private val clientRegistration = mockk<ClientRegistration>()
    private val jwtDecoder = mockk<ReactiveJwtDecoder>()
    private val tokenToExchange = mockk<ResourceAccessJwt>()

    private val underTest = DefaultTokenExchangeService(
        tokenToExchangeValidator,
        authorizationServerApiClient,
        clientRegistration,
        jwtDecoder
    )

    companion object {
        private const val JWT_ACCESS_TOKEN_1 =
            "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJKeUVlaGdoenRIZk5wQzhHRG9oY1V0bGlTREJhOUtlaHRPalI2eDRGZmxFIn0.eyJleHAiOjE2MTUwNjk4NzMsImlzcyI6Imh0dHBzOi8vMTAuMC4wLjE6ODAvYXV0aC9yZWFsbXMvZGV2Iiwic3ViIjoiYjIyODc2ZjUtOTdiYS00MDRhLWJjMTctNjQwMjk0N2NkZGFiIiwidHlwIjoiQmVhcmVyIiwiYXpwIjoidGVzdC1jbGllbnQiLCJzZXNzaW9uX3N0YXRlIjoiZjA0ZTA3YzUtZmQ2My00YmZjLWFlMGItZWM5MDU5MmU2ZDY1In0.PPV5b-RQyqzdZvjKVHPkgPBIwLBqS-09YMg-wKZA1Lzo1givqboEE95U8qNLR9AM69wJsIHUem7vul6nBwPWRO5Cb7_Jo68G0gGs2XrlhzgTFVkbcYlJhkdW_IGRxGDTlM7dhUVezRAY7skTcX6EHVw9EM1FMCkpqwHUb-JHUb2ftUhoVhUzg7bWzkaUE_Uj_a9AlPFg8Paw1L1SL2lg7BD6gr51EWSdkhLafCt0z_PY8-Cs4wWRfc52m72TIZgwwLeSnKsvY6hDr5uhN0RF3k65ylHp-Io0UUhTTC3FHGLXAiHCz8AxBXQmoT7KknBPnupqoMG0Aa8MnmdGSAiuKQ"
        private const val JWT_ACCESS_TOKEN_2 =
            "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c"
        private const val JWT_ACCESS_TOKEN_3 =
            "eyJhbGciOiJIUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJiOWUzZmZlNy1kOWNkLTRhNzAtYjEzMi0xY2NkMThhNDk0ZDkifQ.eyJleHAiOjE2MTU0NjQ0MjUsImlhdCI6MTYxNTQ2MjYyNSwianRpIjoiNTgxNGVlMGEtMjU3Yi00MTdjLWEyMTUtOGFkY2Q1NDZmYzU0IiwiaXNzIjoiaHR0cHM6Ly8xMC4wLjAuMTo4MC9hdXRoL3JlYWxtcy9kZXYiLCJhdWQiOiJodHRwczovLzEwLjAuMC4xOjgwL2F1dGgvcmVhbG1zL2RldiIsInN1YiI6ImE4ODVmZWI5LTYzYTUtNGQ2MC05NDY2LWNmYzdkMzQ2ZDE3MyIsInR5cCI6IlJlZnJlc2giLCJhenAiOiJ0ZXN0LWNsaWVudCIsInNlc3Npb25fc3RhdGUiOiIxYTg2Y2I1OC05MTlkLTRlYjYtOWU0NC0yMTczOTMyYWZmNTkifQ.NWpjM4pccWImFidVpBAKfWPFALc_lDX3mOm3e_E4eFI"
    }

    @Test
    fun `given token to exchange is not valid when token exchange is triggered then TokenToExchangeValidationProblem is thrown`() {
        every { tokenToExchangeValidator.validate(tokenToExchange) } returns failure(OAuth2Error(INVALID_REQUEST))

        assertThat { underTest.exchangeAccessToken(tokenToExchange).block() }
            .isFailure()
            .isInstanceOf(TokenToExchangeValidationProblem::class)

        verify {
            tokenToExchangeValidator.validate(tokenToExchange)
        }
    }

    @Test
    fun `given token to exchange is valid and authorization server returns token with no access token field when token exchange is triggered then ExchangedTokenValidationProblem is thrown`() {
        val issuedFor = "client"
        val tokenValue = "I'm token value"
        val realmName = "dev"
        val clientRegistrationId = "server"
        every { tokenToExchangeValidator.validate(tokenToExchange) } returns success()
        every { tokenToExchange.issuedFor } returns issuedFor
        every { tokenToExchange.tokenValue } returns tokenValue
        every { tokenToExchange.realmName } returns realmName
        every { clientRegistration.clientId } returns clientRegistrationId
        every {
            authorizationServerApiClient.exchangeToken("Bearer $tokenValue", realmName, any())
        } returns JSONObject().apply {
            put("error", INVALID_REQUEST)
            put("error_description", "something wrong happened")
            put("error_uri", "https://tools.ietf.org/html/rfc6750#section-3.1")
        }.toMono()

        assertThat { underTest.exchangeAccessToken(tokenToExchange).block() }
            .isFailure()
            .isInstanceOf(ExchangedTokenValidationProblem::class)

        verify {
            tokenToExchangeValidator.validate(tokenToExchange)
            tokenToExchange.issuedFor
            tokenToExchange.tokenValue
            tokenToExchange.realmName
            clientRegistration.clientId
            authorizationServerApiClient.exchangeToken(
                "Bearer $tokenValue",
                realmName,
                withArg {
                    assertThat(it)
                        .hasSourceClientIdEqualTo(issuedFor)
                        .hasSourceTokenEqualTo(tokenValue)
                        .hasTargetClientIdEqualTo(clientRegistrationId)
                }
            )
        }
    }

    @Test
    fun `given token to exchange is valid and authorization server returns invalid exchanged token when token exchange is triggered then UnparseableExchangedTokenProblem is thrown`() {
        val tokenValue = "token"
        val realmName = "dev"
        every { tokenToExchangeValidator.validate(tokenToExchange) } returns success()
        every { tokenToExchange.issuedFor } returns "client"
        every { tokenToExchange.tokenValue } returns tokenValue
        every { tokenToExchange.realmName } returns realmName
        every { clientRegistration.clientId } returns "server"
        every {
            authorizationServerApiClient.exchangeToken("Bearer $tokenValue", realmName, any())
        } returns JSONObject().apply {
            put("access_token", "invalid token")
            put("expires_in", 300)
            put("refresh_expires_in", 1800)
            put("refresh_token", JWT_ACCESS_TOKEN_3)
            put("token_type", "unknown")
            put("not-before-policy", 0)
            put("session_state", "1a86cb58-919d-4eb6-9e44-2173932aff59")
            put("scope", "profile email")
        }.toMono()

        assertThat { underTest.exchangeAccessToken(tokenToExchange).block() }
            .isFailure()
            .isInstanceOf(UnparseableExchangedTokenProblem::class)

        verify {
            tokenToExchangeValidator.validate(tokenToExchange)
            tokenToExchange.issuedFor
            tokenToExchange.tokenValue
            tokenToExchange.realmName
            clientRegistration.clientId
            authorizationServerApiClient.exchangeToken(
                "Bearer $tokenValue",
                realmName,
                withArg {
                    assertThat(it)
                        .hasSourceClientIdEqualTo("client")
                        .hasSourceTokenEqualTo(tokenValue)
                        .hasTargetClientIdEqualTo("server")
                }
            )
        }
    }

    @Test
    fun `given token to exchange is valid and authorization server returns valid exchanged token when token exchange is triggered then exchanged token is decoded and returned`() {
        val tokenValue = "token"
        val realmName = "dev"
        val exchangedDecodedToken = Jwt(JWT_ACCESS_TOKEN_1,
            LocalDateTime.MIN.toInstant(ZoneOffset.UTC),
            LocalDateTime.MAX.toInstant(ZoneOffset.UTC),
            buildMap<String, String> {
                put(JwtClaimNames.SUB, "a885feb9-63a5-4d60-9466-cfc7d346d173")
            },
            buildMap<String, String> {
                put(JwtClaimNames.ISS, "server")
            })
        every { tokenToExchangeValidator.validate(tokenToExchange) } returns success()
        every { tokenToExchange.issuedFor } returns "client"
        every { tokenToExchange.tokenValue } returns tokenValue
        every { tokenToExchange.realmName } returns realmName
        every { clientRegistration.clientId } returns "server"
        every {
            authorizationServerApiClient.exchangeToken("Bearer $tokenValue", realmName, any())
        } returns JSONObject().apply {
            put("access_token", JWT_ACCESS_TOKEN_2)
            put("expires_in", 300)
            put("refresh_expires_in", 1800)
            put("refresh_token", JWT_ACCESS_TOKEN_3)
            put("token_type", "bearer")
            put("not-before-policy", 0)
            put("session_state", "1a86cb58-919d-4eb6-9e44-2173932aff59")
            put("scope", "profile email")
        }.toMono()
        every { jwtDecoder.decode(JWT_ACCESS_TOKEN_2) } returns exchangedDecodedToken.toMono()

        val exchangedToken = underTest.exchangeAccessToken(tokenToExchange).block()

        assertThat(exchangedToken)
            .isNotNull()
            .hasTokenEqualTo(exchangedDecodedToken.tokenValue)
            .hasIssuedAtEqualTo(exchangedDecodedToken.issuedAt!!)
            .hasExpiresAtEqualTo(exchangedDecodedToken.expiresAt!!)
            .hasIssuedAtEqualTo(exchangedDecodedToken.issuedAt!!)
            .hasHeadersEqualTo(exchangedDecodedToken.headers)
            .hasClaimsEqualTo(exchangedDecodedToken.claims)
        verify {
            tokenToExchangeValidator.validate(tokenToExchange)
            tokenToExchange.issuedFor
            tokenToExchange.tokenValue
            tokenToExchange.realmName
            clientRegistration.clientId
            authorizationServerApiClient.exchangeToken(
                "Bearer $tokenValue",
                realmName,
                withArg {
                    assertThat(it)
                        .hasSourceClientIdEqualTo("client")
                        .hasSourceTokenEqualTo(tokenValue)
                        .hasTargetClientIdEqualTo("server")
                }
            )
            jwtDecoder.decode(JWT_ACCESS_TOKEN_2)
        }
    }
}
