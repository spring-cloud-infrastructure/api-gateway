package com.spring.cloud.oauth2.resource.server.jwt.exchange.validator

import assertk.assertThat
import assertk.assertions.isNotNull
import com.spring.cloud.extension.assertFirstError
import com.spring.cloud.extension.hasAnyErrors
import com.spring.cloud.extension.hasDescriptionEqualTo
import com.spring.cloud.extension.hasErrorCodeEqualTo
import com.spring.cloud.extension.hasErrorsSize
import com.spring.cloud.extension.hasNoErrors
import com.spring.cloud.extension.hasUriEqualTo
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.junit.jupiter.api.Test
import org.springframework.security.oauth2.core.OAuth2ErrorCodes.INVALID_REQUEST
import org.springframework.security.oauth2.jwt.Jwt
import java.time.Clock
import java.time.Duration
import java.time.LocalDateTime
import java.time.ZoneOffset

internal class JwtExpirationTimeValidatorTest {

    private val clock = mockk<Clock>()
    private val token = mockk<Jwt>()

    @Test
    internal fun `given token has minimal JWT expiration time when token validation is triggered then validation ends with success`() {
        val minimalJwtExpirationTime = Duration.ofDays(3)
        val currentTime = LocalDateTime.now()
        val expiresAt = currentTime.plus(minimalJwtExpirationTime)
        every { clock.instant() } returns currentTime.toInstant(ZoneOffset.UTC)
        every { token.expiresAt } returns expiresAt.toInstant(ZoneOffset.UTC)

        val validatorResult = getUnderTest(minimalJwtExpirationTime).validate(token)

        assertThat(validatorResult)
            .isNotNull()
            .hasNoErrors()
        verify {
            token.expiresAt
            clock.instant()
        }
    }

    @Test
    internal fun `given token has enough JWT expiration time when token validation is triggered then validation ends with success`() {
        val minimalJwtExpirationTime = Duration.ofDays(3)
        val currentTime = LocalDateTime.now()
        val expiresAt = currentTime.plus(minimalJwtExpirationTime).plusSeconds(1)
        every { clock.instant() } returns currentTime.toInstant(ZoneOffset.UTC)
        every { token.expiresAt } returns expiresAt.toInstant(ZoneOffset.UTC)

        val validatorResult = getUnderTest(minimalJwtExpirationTime).validate(token)

        assertThat(validatorResult)
            .isNotNull()
            .hasNoErrors()
        verify {
            clock.instant()
            token.expiresAt
        }
    }

    @Test
    internal fun `given token is not longer valid when token validation is triggered then validation ends with error`() {
        val minimalJwtExpirationTime = Duration.ofDays(3)
        val currentTime = LocalDateTime.now()
        val expiresAt = currentTime.plus(minimalJwtExpirationTime).minusSeconds(1)
        every { clock.instant() } returns currentTime.toInstant(ZoneOffset.UTC)
        every { token.expiresAt } returns expiresAt.toInstant(ZoneOffset.UTC)

        val validatorResult = getUnderTest(minimalJwtExpirationTime).validate(token)

        assertThat(validatorResult)
            .isNotNull()
            .hasAnyErrors()
            .hasErrorsSize(1)
            .assertFirstError()
            .hasErrorCodeEqualTo(INVALID_REQUEST)
            .hasDescriptionEqualTo("Minimal JWT expiration time [$minimalJwtExpirationTime] is not fulfilled, expiration time: [${
                expiresAt.toInstant(ZoneOffset.UTC)
            }]")
            .hasUriEqualTo("https://tools.ietf.org/html/rfc6750#section-3.1")
        verify {
            clock.instant()
            token.expiresAt
        }
    }

    @Test
    internal fun `given token has no expiry defined when token validation is triggered then validation ends with error`() {
        val minimalJwtExpirationTime = Duration.ofDays(3)
        every { token.expiresAt } returns null

        val validatorResult = getUnderTest(minimalJwtExpirationTime).validate(token)

        assertThat(validatorResult)
            .isNotNull()
            .hasAnyErrors()
            .hasErrorsSize(1)
            .assertFirstError()
            .hasErrorCodeEqualTo(INVALID_REQUEST)
            .hasDescriptionEqualTo("Minimal JWT expiration time [$minimalJwtExpirationTime] is not fulfilled, expiration time: [null]")
            .hasUriEqualTo("https://tools.ietf.org/html/rfc6750#section-3.1")
        verify {
            token.expiresAt
        }
    }

    private fun getUnderTest(minimalExpirationTime: Duration) = JwtExpirationTimeValidator(clock, minimalExpirationTime)
}
