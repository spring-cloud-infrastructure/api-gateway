package com.spring.cloud.oauth2.resource.server.jwt.provider

import assertk.assertThat
import assertk.assertions.isNotNull
import com.github.tomakehurst.wiremock.WireMockServer
import com.github.tomakehurst.wiremock.client.WireMock.equalTo
import com.github.tomakehurst.wiremock.client.WireMock.postRequestedFor
import com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo
import com.marcinziolo.kotlin.wiremock.equalTo
import com.marcinziolo.kotlin.wiremock.post
import com.marcinziolo.kotlin.wiremock.returns
import com.spring.cloud.ApiGatewayApplication
import com.spring.cloud.TestLoadBalancerConfiguration
import com.spring.cloud.extension.hasAccessTokenEqualTo
import com.spring.cloud.extension.hasExpiresInEqualTo
import com.spring.cloud.extension.hasNotBeforePolicyEqualTo
import com.spring.cloud.extension.hasRefreshExpiresInEqualTo
import com.spring.cloud.extension.hasRefreshTokenEqualTo
import com.spring.cloud.extension.hasScopeEqualTo
import com.spring.cloud.extension.hasSessionStateEqualTo
import com.spring.cloud.extension.hasTokenTypeEqualTo
import com.spring.cloud.oauth2.resource.server.jwt.provider.model.TokenExchangeRequest
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock
import org.springframework.http.HttpHeaders.ACCEPT
import org.springframework.http.HttpHeaders.AUTHORIZATION
import org.springframework.http.HttpHeaders.CONTENT_TYPE
import org.springframework.http.MediaType.APPLICATION_FORM_URLENCODED_VALUE
import org.springframework.http.MediaType.APPLICATION_JSON_VALUE
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig

@SpringBootTest(
    webEnvironment = RANDOM_PORT,
    classes = [
        ApiGatewayApplication::class
    ]
)
@AutoConfigureMockMvc
@AutoConfigureWireMock
@SpringJUnitConfig(TestLoadBalancerConfiguration::class)
internal class AuthorizationServerApiClientIntegrationTest(
    @Autowired private val underTest: AuthorizationServerApiClient,
    @Autowired private val wiremock: WireMockServer,
) {

    companion object {
        private const val REALM_NAME = "test"
        private const val AUTHORIZATION_SERVER_TOKEN_URL = "/auth/realms/$REALM_NAME/protocol/openid-connect/token"
        private const val JWT_ACCESS_TOKEN =
            "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJKeUVlaGdoenRIZk5wQzhHRG9oY1V0bGlTREJhOUtlaHRPalI2eDRGZmxFIn0.eyJleHAiOjE2MTUwNjk4NzMsImlzcyI6Imh0dHBzOi8vMTAuMC4wLjE6ODAvYXV0aC9yZWFsbXMvZGV2Iiwic3ViIjoiYjIyODc2ZjUtOTdiYS00MDRhLWJjMTctNjQwMjk0N2NkZGFiIiwidHlwIjoiQmVhcmVyIiwiYXpwIjoidGVzdC1jbGllbnQiLCJzZXNzaW9uX3N0YXRlIjoiZjA0ZTA3YzUtZmQ2My00YmZjLWFlMGItZWM5MDU5MmU2ZDY1In0.PPV5b-RQyqzdZvjKVHPkgPBIwLBqS-09YMg-wKZA1Lzo1givqboEE95U8qNLR9AM69wJsIHUem7vul6nBwPWRO5Cb7_Jo68G0gGs2XrlhzgTFVkbcYlJhkdW_IGRxGDTlM7dhUVezRAY7skTcX6EHVw9EM1FMCkpqwHUb-JHUb2ftUhoVhUzg7bWzkaUE_Uj_a9AlPFg8Paw1L1SL2lg7BD6gr51EWSdkhLafCt0z_PY8-Cs4wWRfc52m72TIZgwwLeSnKsvY6hDr5uhN0RF3k65ylHp-Io0UUhTTC3FHGLXAiHCz8AxBXQmoT7KknBPnupqoMG0Aa8MnmdGSAiuKQ"
        private const val JWT_REFRESH_TOKEN =
            """eyJhbGciOiJIUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJiOWUzZmZlNy1kOWNkLTRhNzAtYjEzMi0xY2NkMThhNDk0ZDkifQ.eyJleHAiOjE2MTU0NjQ1OTksImlhdCI6MTYxNTQ2Mjc5OSwianRpIjoiOGRkY2I0MjUtNzgxMi00NDg2LTg2NzctNGY1OThhYjg0ZGFhIiwiaXNzIjoiaHR0cHM6Ly8xMC4wLjAuMTo4MC9hdXRoL3JlYWxtcy9kZXYiLCJhdWQiOiJodHRwczovLzEwLjAuMC4xOjgwL2F1dGgvcmVhbG1zL2RldiIsInN1YiI6ImE4ODVmZWI5LTYzYTUtNGQ2MC05NDY2LWNmYzdkMzQ2ZDE3MyIsInR5cCI6IlJlZnJlc2giLCJhenAiOiJ0ZXN0LWNsaWVudCIsInNlc3Npb25fc3RhdGUiOiJjYjIzNmE3MS0wNzA0LTRjMTgtODgzNS02YjVlMjQ5Mjk5YmUifQ.RZgGsbymHdGyOWb4SSXl5VeF6Plxmsy8vq6T11_l-cc"""
    }

    @Test
    fun `given authorization server API client when exchange token endpoint is getting invoked then response is returned`() {
        val tokenExchangeRequest = TokenExchangeRequest(
            sourceClientId = "client",
            sourceToken = JWT_ACCESS_TOKEN,
            targetClientId = "server"
        )
        wiremock.post { url equalTo AUTHORIZATION_SERVER_TOKEN_URL } returns {
            body = """
            {
                "access_token": "$JWT_ACCESS_TOKEN",
                "expires_in": 300,
                "refresh_expires_in": 1800,
                "refresh_token": "$JWT_REFRESH_TOKEN",
                "token_type": "bearer",
                "not-before-policy": 0,
                "session_state": "cb236a71-0704-4c18-8835-6b5e249299be",
                "scope": "profile email"
            }"""
            header = CONTENT_TYPE to APPLICATION_JSON_VALUE
        }

        val exchangeToken = underTest.exchangeToken(
            "Bearer $JWT_ACCESS_TOKEN",
            REALM_NAME,
            tokenExchangeRequest
        ).block()

        assertThat(exchangeToken)
            .isNotNull()
            .hasAccessTokenEqualTo(JWT_ACCESS_TOKEN)
            .hasExpiresInEqualTo(300)
            .hasRefreshTokenEqualTo(JWT_REFRESH_TOKEN)
            .hasRefreshExpiresInEqualTo(1800)
            .hasTokenTypeEqualTo("bearer")
            .hasNotBeforePolicyEqualTo(0)
            .hasSessionStateEqualTo("cb236a71-0704-4c18-8835-6b5e249299be")
            .hasScopeEqualTo("profile email")
        wiremock.verify(
            postRequestedFor(urlEqualTo(AUTHORIZATION_SERVER_TOKEN_URL))
                .withHeader(AUTHORIZATION, equalTo("Bearer $JWT_ACCESS_TOKEN"))
                .withHeader(ACCEPT, equalTo(APPLICATION_JSON_VALUE))
                .withHeader(CONTENT_TYPE, equalTo("$APPLICATION_FORM_URLENCODED_VALUE;charset=UTF-8"))
                .withRequestBody(
                    equalTo(
                        "grant_type=urn%3Aietf%3Aparams%3Aoauth%3Agrant-type%3Atoken-exchange&" +
                                "client_id=${tokenExchangeRequest.sourceClientId}&" +
                                "subject_token=$JWT_ACCESS_TOKEN&" +
                                "subject_token_type=urn%3Aietf%3Aparams%3Aoauth%3Atoken-type%3Aaccess_token&" +
                                "audience=${tokenExchangeRequest.targetClientId}&" +
                                "requested_token_type=urn%3Aietf%3Aparams%3Aoauth%3Atoken-type%3Aaccess_token"
                    )
                )
        )
    }
}
