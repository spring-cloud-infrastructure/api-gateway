package com.spring.cloud.oauth2.resource.server.jwt.exchange.validator

import assertk.assertThat
import assertk.assertions.isNotNull
import com.spring.cloud.config.oauth2.resource.server.jwt.exchange.TokenExchangeProperties
import com.spring.cloud.extension.assertFirstError
import com.spring.cloud.extension.hasAnyErrors
import com.spring.cloud.extension.hasDescriptionEqualTo
import com.spring.cloud.extension.hasErrorCodeEqualTo
import com.spring.cloud.extension.hasErrorsEqualTo
import com.spring.cloud.extension.hasErrorsSize
import com.spring.cloud.extension.hasNoErrors
import com.spring.cloud.extension.hasUriEqualTo
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.junit.jupiter.api.Test
import org.springframework.security.oauth2.core.OAuth2Error
import org.springframework.security.oauth2.core.OAuth2ErrorCodes.INVALID_REQUEST
import org.springframework.security.oauth2.core.OAuth2TokenValidatorResult.failure
import org.springframework.security.oauth2.core.OAuth2TokenValidatorResult.success
import org.springframework.security.oauth2.jwt.Jwt
import java.time.Clock
import java.time.Duration
import java.time.LocalDateTime
import java.time.ZoneOffset.UTC

internal class TokenToExchangeValidatorTest {

    private val clock = mockk<Clock>()
    private val tokenExchangeProperties = mockk<TokenExchangeProperties>()
    private val jwtIssuerUriValidator = mockk<JwtIssuerUriValidator>()
    private val token = mockk<Jwt>()

    @Test
    fun `given token has no valid issuer uri when token validation is triggered then validation fails with failure result`() {
        val minimalJwtExpirationTime = Duration.ofDays(3)
        val currentTime = LocalDateTime.now()
        val expiresAt = currentTime.plus(minimalJwtExpirationTime)
        every { clock.instant() } returns currentTime.toInstant(UTC)
        every { token.expiresAt } returns expiresAt.toInstant(UTC)
        every { tokenExchangeProperties.minimalJwtExpirationTime } returns minimalJwtExpirationTime
        val issuerValidationResult = failure(OAuth2Error(INVALID_REQUEST))
        every { tokenExchangeProperties.requiredClaims } returns emptySet<String>().toMutableSet()
        every { jwtIssuerUriValidator.validate(token) } returns issuerValidationResult

        val validatorResult = getUnderTest().validate(token)

        assertThat(validatorResult)
            .isNotNull()
            .hasAnyErrors()
            .hasErrorsEqualTo(issuerValidationResult.errors)
        verify {
            clock.instant()
            tokenExchangeProperties.minimalJwtExpirationTime
            tokenExchangeProperties.requiredClaims
            jwtIssuerUriValidator.validate(token)
            token.expiresAt
        }
    }

    @Test
    fun `given token has invalid token expiration time when token validation is triggered then validation fails with failure result`() {
        val minimalJwtExpirationTime = Duration.ofDays(3)
        val currentTime = LocalDateTime.now()
        val expiresAt = currentTime.plus(minimalJwtExpirationTime).minusSeconds(1)
        every { clock.instant() } returns currentTime.toInstant(UTC)
        every { token.expiresAt } returns expiresAt.toInstant(UTC)
        every { tokenExchangeProperties.minimalJwtExpirationTime } returns minimalJwtExpirationTime
        every { tokenExchangeProperties.requiredClaims } returns emptySet<String>().toMutableSet()
        every { jwtIssuerUriValidator.validate(token) } returns success()

        val validatorResult = getUnderTest().validate(token)

        assertThat(validatorResult)
            .isNotNull()
            .hasAnyErrors()
            .hasErrorsSize(1)
            .assertFirstError()
            .hasErrorCodeEqualTo(INVALID_REQUEST)
            .hasDescriptionEqualTo("Minimal JWT expiration time [$minimalJwtExpirationTime] is not fulfilled, expiration time: [${
                expiresAt.toInstant(UTC)
            }]")
            .hasUriEqualTo("https://tools.ietf.org/html/rfc6750#section-3.1")
        verify {
            clock.instant()
            tokenExchangeProperties.minimalJwtExpirationTime
            tokenExchangeProperties.requiredClaims
            jwtIssuerUriValidator.validate(token)
            token.expiresAt
        }
    }

    @Test
    fun `given token misses required claims when token validation is triggered then validation fails with failure result`() {
        val minimalJwtExpirationTime = Duration.ofDays(3)
        val currentTime = LocalDateTime.now()
        val expiresAt = currentTime.plus(minimalJwtExpirationTime)
        every { clock.instant() } returns currentTime.toInstant(UTC)
        every { token.expiresAt } returns expiresAt.toInstant(UTC)
        every { tokenExchangeProperties.minimalJwtExpirationTime } returns minimalJwtExpirationTime
        val requiredClaimName = "claim1"
        every { tokenExchangeProperties.requiredClaims } returns mutableSetOf(requiredClaimName)
        every { token.getClaim<String>(requiredClaimName) } returns null
        every { jwtIssuerUriValidator.validate(token) } returns success()

        val validatorResult = getUnderTest().validate(token)

        assertThat(validatorResult)
            .isNotNull()
            .hasAnyErrors()
            .hasErrorsSize(1)
            .assertFirstError()
            .hasErrorCodeEqualTo(INVALID_REQUEST)
            .hasDescriptionEqualTo("The $requiredClaimName claim is not valid")
            .hasUriEqualTo("https://tools.ietf.org/html/rfc6750#section-3.1")
        verify {
            clock.instant()
            tokenExchangeProperties.minimalJwtExpirationTime
            tokenExchangeProperties.requiredClaims
            token.getClaim<String>(requiredClaimName)
            jwtIssuerUriValidator.validate(token)
            token.expiresAt
        }
    }

    @Test
    fun `given token is valid when token validation is triggered then validation ends with success result`() {
        val minimalJwtExpirationTime = Duration.ofDays(3)
        val currentTime = LocalDateTime.now()
        val expiresAt = currentTime.plus(minimalJwtExpirationTime)
        every { clock.instant() } returns currentTime.toInstant(UTC)
        every { token.expiresAt } returns expiresAt.toInstant(UTC)
        every { tokenExchangeProperties.minimalJwtExpirationTime } returns minimalJwtExpirationTime
        val requiredClaimName = "claim1"
        every { tokenExchangeProperties.requiredClaims } returns mutableSetOf(requiredClaimName)
        every { token.getClaim<String>(requiredClaimName) } returns "claim-1-value"
        every { jwtIssuerUriValidator.validate(token) } returns success()

        val validatorResult = getUnderTest().validate(token)

        assertThat(validatorResult)
            .isNotNull()
            .hasNoErrors()
        verify {
            clock.instant()
            tokenExchangeProperties.minimalJwtExpirationTime
            tokenExchangeProperties.requiredClaims
            token.getClaim<String>(requiredClaimName)
            jwtIssuerUriValidator.validate(token)
            token.expiresAt
        }
    }

    private fun getUnderTest() = TokenToExchangeValidator(clock, tokenExchangeProperties, jwtIssuerUriValidator)
}
