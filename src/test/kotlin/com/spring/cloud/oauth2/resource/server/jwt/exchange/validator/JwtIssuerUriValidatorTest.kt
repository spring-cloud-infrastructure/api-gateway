package com.spring.cloud.oauth2.resource.server.jwt.exchange.validator

import assertk.assertThat
import assertk.assertions.isNotNull
import com.spring.cloud.config.oauth2.resource.server.jwt.decoder.MultiTenantOauth2ResourceServerProperties
import com.spring.cloud.extension.assertFirstError
import com.spring.cloud.extension.hasAnyErrors
import com.spring.cloud.extension.hasDescriptionEqualTo
import com.spring.cloud.extension.hasErrorCodeEqualTo
import com.spring.cloud.extension.hasErrorsSize
import com.spring.cloud.extension.hasNoErrors
import com.spring.cloud.extension.hasUriEqualTo
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.junit.jupiter.api.Test
import org.springframework.boot.autoconfigure.security.oauth2.resource.OAuth2ResourceServerProperties
import org.springframework.security.oauth2.core.OAuth2ErrorCodes.INVALID_REQUEST
import org.springframework.security.oauth2.jwt.Jwt
import java.net.URL

internal class JwtIssuerUriValidatorTest {

    private val resourceServerProperties = MultiTenantOauth2ResourceServerProperties()
    private val token = mockk<Jwt>()

    private val underTest = JwtIssuerUriValidator(resourceServerProperties)

    @Test
    fun `given token has no issuer when token issuer uri validation is triggered then validation fails with failure result`() {
        every { token.issuer } returns null

        val validatorResult = underTest.validate(token)

        assertThat(validatorResult)
            .isNotNull()
            .hasAnyErrors()
            .hasErrorsSize(1)
            .assertFirstError()
            .hasErrorCodeEqualTo(INVALID_REQUEST)
            .hasDescriptionEqualTo("JWT issuer: [null] is unknown")
            .hasUriEqualTo("https://tools.ietf.org/html/rfc6750#section-3.1")
        verify {
            token.issuer
        }
    }

    @Test
    fun `given token has unknown issuer when token issuer uri validation is triggered then validation fails with failure result`() {
        val issuer = "http://localhost:8080/issuer1"
        every { token.issuer } returns URL(issuer)
        resourceServerProperties.add(OAuth2ResourceServerProperties.Jwt().apply {
            issuerUri = "http://localhost:8080/issuer2"
        })

        val validatorResult = underTest.validate(token)

        assertThat(validatorResult)
            .isNotNull()
            .hasAnyErrors()
            .hasErrorsSize(1)
            .assertFirstError()
            .hasErrorCodeEqualTo(INVALID_REQUEST)
            .hasDescriptionEqualTo("JWT issuer: [$issuer] is unknown")
            .hasUriEqualTo("https://tools.ietf.org/html/rfc6750#section-3.1")
        verify {
            token.issuer
        }
    }

    @Test
    fun `given token has known issuer when token issuer uri validation is triggered then validation ends with success result`() {
        every { token.issuer } returns URL("http://localhost:8080/issuer1")
        resourceServerProperties.add(OAuth2ResourceServerProperties.Jwt().apply {
            issuerUri = "http://localhost:8080/ISSUER1"
        })

        val validatorResult = underTest.validate(token)

        assertThat(validatorResult)
            .isNotNull()
            .hasNoErrors()
        verify {
            token.issuer
        }
    }
}
