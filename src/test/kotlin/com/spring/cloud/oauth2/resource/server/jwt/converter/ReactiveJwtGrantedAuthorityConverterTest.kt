package com.spring.cloud.oauth2.resource.server.jwt.converter

import assertk.assertThat
import assertk.assertions.containsExactlyInAnyOrder
import assertk.assertions.isEmpty
import assertk.assertions.isNotNull
import com.spring.cloud.oauth2.resource.server.jwt.model.JwtGrantedAuthority
import com.spring.cloud.oauth2.resource.server.jwt.model.ResourceAccess
import com.spring.cloud.oauth2.resource.server.jwt.model.ResourceAccessJwt
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.junit.jupiter.api.Test

internal class ReactiveJwtGrantedAuthorityConverterTest {

    private val resourceAccessJwt = mockk<ResourceAccessJwt>()

    private val underTest = ReactiveJwtGrantedAuthorityConverter()

    @Test
    @ExperimentalStdlibApi
    fun `given resource access JWT has resource access claim with roles when mapping to granted authorities is triggered then resource access claim is mapped to granted authorities`() {
        every { resourceAccessJwt.resourceAccess } returns buildSet {
            add(ResourceAccess(
                resourceName = "service-a",
                roles = buildSet {
                    add("role1")
                    add("role2")
                    add("role3")
                }
            ))

            add(ResourceAccess(
                resourceName = "service-b",
                roles = buildSet {
                    add("role4")
                    add("role5")
                    add("role6")
                }
            ))
        }

        val grantedAuthorities = underTest.convert(resourceAccessJwt).block()

        assertThat(grantedAuthorities).isNotNull()
        assertThat(grantedAuthorities!!.map(JwtGrantedAuthority::authorityWithNoScopePrefix)).containsExactlyInAnyOrder(
            "SERVICE-A:ROLE1",
            "SERVICE-A:ROLE2",
            "SERVICE-A:ROLE3",
            "SERVICE-B:ROLE4",
            "SERVICE-B:ROLE5",
            "SERVICE-B:ROLE6"
        )
        verify {
            resourceAccessJwt.resourceAccess
        }
    }

    @Test
    fun `given resource access JWT has resource access claim with no roles when mapping to granted authorities is triggered then no granted authorities are mapped`() {
        every { resourceAccessJwt.resourceAccess } returns emptySet()

        val grantedAuthorities = underTest.convert(resourceAccessJwt).block()

        assertThat(grantedAuthorities).isNotNull()
        assertThat(grantedAuthorities!!).isEmpty()
        verify {
            resourceAccessJwt.resourceAccess
        }
    }
}
