package com.spring.cloud.oauth2.resource.server.jwt.decoder

import assertk.assertThat
import assertk.assertions.isFailure
import assertk.assertions.isInstanceOf
import assertk.assertions.isNotNull
import com.spring.cloud.config.oauth2.resource.server.jwt.decoder.MultiTenantOauth2ResourceServerProperties
import com.spring.cloud.problem.NoSuitableDecoderFoundProblem
import com.spring.cloud.problem.UnparseableTokenProblem
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.junit.jupiter.api.Test
import org.springframework.boot.autoconfigure.security.oauth2.resource.OAuth2ResourceServerProperties.Jwt
import org.springframework.web.reactive.function.client.WebClient
import org.springframework.web.reactive.function.client.WebClient.RequestHeadersUriSpec
import org.springframework.web.reactive.function.client.WebClient.ResponseSpec
import reactor.kotlin.core.publisher.toMono
import java.time.Clock
import java.time.LocalDate
import java.time.ZoneOffset.UTC

internal class MultiTenantReactiveJwtDecoderTest {

    private val clock = mockk<Clock>()
    private val jwtProperties = mockk<Jwt>()
    private val resourceServerProperties = MultiTenantOauth2ResourceServerProperties().apply {
        add(jwtProperties)
    }
    private val webClient = mockk<WebClient>()
    private val requestHeadersUriSpec = mockk<RequestHeadersUriSpec<*>>()
    private val responseSpec = mockk<ResponseSpec>()

    companion object {
        private const val JWT_ACCESS_TOKEN =
            "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJOYXhtNGg1YjNkMHF6ak1DZ2FpNlhBYmxSQWhDc2xCZ1MydktwOUVmdURjIn0.eyJleHAiOjE2MTU1MDg5NzEsImlzcyI6Imh0dHBzOi8vMTAuMC4wLjE6ODAvYXV0aC9yZWFsbXMvZGV2IiwiYXVkIjpudWxsLCJzdWIiOiJhODg1ZmViOS02M2E1LTRkNjAtOTQ2Ni1jZmM3ZDM0NmQxNzMiLCJ0eXAiOiJCZWFyZXIiLCJhenAiOiJ0ZXN0LWNsaWVudCIsInNlc3Npb25fc3RhdGUiOiJkMzAxNThlMC0wMTEyLTQ1NjYtOGQwOC1lYTRiYWRmNDE5OTQifQ.SPFDzS70-9CG7-l_fG81BRLIsgChkgjHnJlrxsntlSmwc_S9T1z-deD9Xb7lfhD-f8nUVIwNobFF8jsXbKWTq0bXGOu9py2jU3PTHAOUr79BiFuhtmLmnXhGme6VUlTcbcF5w2CY-jnEeHkqJbl8vIxbfffaF_VKImBe1IaWMAJpjQTjwF4R4KfenHfXfmZTUqSBcDXyCDXU5XpKNPtEnIZdzWlmjursA2Eo3K5PKeJVbnsH7SoCmPyikbh0qw79eh6mOTUYclOEtggttbAUoF5o9RJfHasmYUWMy8WQGYqPtwLzwI_y9yb6sY2xzfKj8eZVBfqZxL2G_anNToiEjg"
    }

    @Test
    fun `given jwt decoder for issuer uri found when token is getting decoded then token is decoded with right jwt decoder`() {
        val jwkSetUri = "https://10.0.0.1:80/token"
        every { jwtProperties.jwkSetUri } returns jwkSetUri
        every { jwtProperties.issuerUri } returns "https://10.0.0.1:80/auth/realms/dev"
        every { webClient.get() } returns requestHeadersUriSpec
        every { requestHeadersUriSpec.uri(jwkSetUri) } returns requestHeadersUriSpec
        every { requestHeadersUriSpec.retrieve() } returns responseSpec
        every { responseSpec.bodyToMono(String::class.java) } returns """
            {
              "keys": [
                {
                  "kty": "RSA",
                  "x5t#S256": "s1iHpn8o7hXhl-MPWXD4kZkfEA9yMlXRexitnHP9IGk",
                  "e": "AQAB",
                  "use": "sig",
                  "x5t": "cwI2UMSrHbjp83J1srsVKYkTGYA",
                  "kid": "Naxm4h5b3d0qzjMCgai6XAblRAhCslBgS2vKp9EfuDc",
                  "x5c": [
                    "MIIClTCCAX0CBgF4FBmoATANBgkqhkiG9w0BAQsFADAOMQwwCgYDVQQDDANkZXYwHhcNMjEwMzA4MjMwNzA5WhcNMzEwMzA4MjMwODQ5WjAOMQwwCgYDVQQDDANkZXYwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQCOW/q3rM4jFwk5ZuBv0aZjYeiMGI3z7Rzm/TwJ5jnmmPdL5XBnK8ivoaTS0Ma+0ywneGKUE4lcDUgxhs/QymtKunSzbaGdGO5xO7fjKa6if6Y5D9v/srV6qFdaTaA6e+QYZmW8yzhqYIReFs41kViu69CXFrbdvDMFlYwHlVlhoHr1rJmvklNifAxbuiRr406TAtxq89S64FQbwSLHst/Ir5upNn+7YXkKOB9Nw5TOrLt1aroP1a/iyqRVDJFlJhBWd3vPNr9ko+zD/5VsNvDauclQjkHUky/iFVGqlBFnaAMTyIWw4FoCfgSYyn1I3TgdS8mvEhgE574vTuto+dx3AgMBAAEwDQYJKoZIhvcNAQELBQADggEBABI3ruxPE16q2EAe6WdZKTSpREnQZRhXpt+ebQsUKn5N0jIVjxGeKLn5DihgzAMBwBEHCLZWkGb4+bvFdTE7Bqxw9UV3oULy9RAWbdMvmV8+3zNskL322AhYy123SFxM0WCoLWu3xHT33TTz+oTgCgN04blvto48B55+Wg+uK4mvgmdwL/xlqJ144gvCo+LQj+Rl6Vrwbap43aVqeo//AVF9hvW7Fk+ABK8O0jyDCPr6ZjJ7LdZt3+MoDMNV46RrnqxL16iU/DM4yi0D1SP7k5VgfNM5zG7JPoQx+9Wu1I5a2lAZvZ0Zxw0619eW5QZ4rVIxeTl4KwpF0fgYNbMgiRk="
                  ],
                  "alg": "RS256",
                  "n": "jlv6t6zOIxcJOWbgb9GmY2HojBiN8-0c5v08CeY55pj3S-VwZyvIr6Gk0tDGvtMsJ3hilBOJXA1IMYbP0MprSrp0s22hnRjucTu34ymuon-mOQ_b_7K1eqhXWk2gOnvkGGZlvMs4amCEXhbONZFYruvQlxa23bwzBZWMB5VZYaB69ayZr5JTYnwMW7oka-NOkwLcavPUuuBUG8Eix7LfyK-bqTZ_u2F5CjgfTcOUzqy7dWq6D9Wv4sqkVQyRZSYQVnd7zza_ZKPsw_-VbDbw2rnJUI5B1JMv4hVRqpQRZ2gDE8iFsOBaAn4EmMp9SN04HUvJrxIYBOe-L07raPncdw"
                }
              ]
            }""".toMono()
        every { clock.instant() } returns LocalDate.of(2021, 3, 11).atTime(15, 40).toInstant(UTC)

        val token = getUnderTest().decode(JWT_ACCESS_TOKEN).block()

        assertThat(token).isNotNull()

        verify {
            jwtProperties.jwkSetUri
            jwtProperties.issuerUri
            webClient.get()
            requestHeadersUriSpec.uri(jwkSetUri)
            requestHeadersUriSpec.retrieve()
            responseSpec.bodyToMono(String::class.java)
        }
    }

    @Test
    fun `given jwt decoder for issuer uri not found when token is getting decoded then NoSuitableDecoderFoundProblem is thrown`() {
        every { jwtProperties.jwkSetUri } returns "https://10.0.0.1:80/token"
        every { jwtProperties.issuerUri } returns "https://10.0.0.1:80/auth/realms/master"

        assertThat { getUnderTest().decode(JWT_ACCESS_TOKEN).block() }
            .isFailure()
            .isInstanceOf(NoSuitableDecoderFoundProblem::class)

        verify {
            jwtProperties.jwkSetUri
            jwtProperties.issuerUri
        }
    }

    @Test
    fun `given JWT token is not parsable when token is getting decoded then UnparseableTokenProblem is thrown`() {
        every { jwtProperties.jwkSetUri } returns "https://10.0.0.1:80/token"
        every { jwtProperties.issuerUri } returns "https://10.0.0.1:80/auth/realms/master"

        assertThat { getUnderTest().decode("fake token 123").block() }
            .isFailure()
            .isInstanceOf(UnparseableTokenProblem::class)

        verify {
            jwtProperties.jwkSetUri
            jwtProperties.issuerUri
        }
    }

    private fun getUnderTest() = MultiTenantReactiveJwtDecoder(clock, resourceServerProperties, webClient)
}
