package com.spring.cloud.oauth2.resource.server.jwt.exchange

import assertk.assertThat
import assertk.assertions.isEqualTo
import com.spring.cloud.config.oauth2.resource.server.jwt.exchange.TokenExchangeProperties
import com.spring.cloud.config.oauth2.resource.server.jwt.exchange.TokenExchangeProperties.TokenExchangeCacheProperties
import com.spring.cloud.oauth2.resource.server.jwt.exchange.validator.CachedTokenValidator
import com.spring.cloud.oauth2.resource.server.jwt.model.ResourceAccessJwt
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.junit.jupiter.api.Test
import org.springframework.cache.Cache
import org.springframework.cache.Cache.ValueWrapper
import org.springframework.cache.CacheManager
import org.springframework.security.oauth2.core.OAuth2Error
import org.springframework.security.oauth2.core.OAuth2ErrorCodes.INVALID_REQUEST
import org.springframework.security.oauth2.core.OAuth2TokenValidatorResult.failure
import org.springframework.security.oauth2.core.OAuth2TokenValidatorResult.success
import reactor.kotlin.core.publisher.toMono
import java.time.LocalDateTime
import java.time.ZoneOffset.UTC

internal class CachedTokenExchangeServiceTest {

    private val tokenExchangeProperties = mockk<TokenExchangeProperties>()
    private val cacheManager = mockk<CacheManager>()
    private val defaultTokenExchangeService = mockk<TokenExchangeService>()
    private val cachedTokenValidator = mockk<CachedTokenValidator>()
    private val cacheProperties = mockk<TokenExchangeCacheProperties>()
    private val cache = mockk<Cache>()
    private val tokenToExchange = mockk<ResourceAccessJwt>()
    private val tokenInCache = mockk<ResourceAccessJwt>()
    private val exchangedToken = mockk<ResourceAccessJwt>()

    private val underTest = CachedTokenExchangeService(
        tokenExchangeProperties,
        cacheManager,
        defaultTokenExchangeService,
        cachedTokenValidator
    )

    @Test
    fun `given token not found in cache when token exchange is triggered then token is exchanged`() {
        val tokenSubject = "client-1"
        val cacheName = "token-exchange-cache"
        every { tokenToExchange.subject } returns tokenSubject
        every { tokenExchangeProperties.cache } returns cacheProperties
        every { cacheProperties.name } returns cacheName
        every { cacheManager.getCache(cacheName) } returns null
        every { defaultTokenExchangeService.exchangeAccessToken(tokenToExchange) } returns exchangedToken.toMono()

        assertThat(underTest.exchangeAccessToken(tokenToExchange).block()).isEqualTo(exchangedToken)

        verify {
            tokenToExchange.subject
            tokenExchangeProperties.cache
            cacheProperties.name
            cacheManager.getCache(cacheName)
            defaultTokenExchangeService.exchangeAccessToken(tokenToExchange)
        }
    }

    @Test
    fun `given token not found in cache when token exchange is triggered then token is exchanged and exchange token is stored in cache`() {
        val tokenSubject = "client-1"
        val cacheName = "token-exchange-cache"
        every { tokenToExchange.subject } returns tokenSubject
        every { tokenExchangeProperties.cache } returns cacheProperties
        every { cacheProperties.name } returns cacheName
        every { cacheManager.getCache(cacheName) } returnsMany listOf(null, cache)
        every { defaultTokenExchangeService.exchangeAccessToken(tokenToExchange) } returns exchangedToken.toMono()
        every { exchangedToken.expiresAt } returns LocalDateTime.now().toInstant(UTC)
        every { cache.put(tokenSubject, exchangedToken) } returns Unit

        assertThat(underTest.exchangeAccessToken(tokenToExchange).block()).isEqualTo(exchangedToken)

        verify {
            tokenToExchange.subject
            defaultTokenExchangeService.exchangeAccessToken(tokenToExchange)
            exchangedToken.expiresAt
            cache.put(tokenSubject, exchangedToken)
            tokenExchangeProperties.cache
            cacheProperties.name
            cacheManager.getCache(cacheName)
        }
    }

    @Test
    fun `given invalid token found in cache when token exchange is triggered then token is exchanged`() {
        val tokenSubject = "client-1"
        val cacheName = "token-exchange-cache"
        every { tokenToExchange.subject } returns tokenSubject
        every { tokenExchangeProperties.cache } returns cacheProperties
        every { cacheProperties.name } returns cacheName
        every { cacheManager.getCache(cacheName) } returnsMany listOf(cache, null)
        every { cache[tokenSubject] } returns ValueWrapper { tokenInCache }
        every { cachedTokenValidator.validate(tokenInCache) } returns failure(OAuth2Error(INVALID_REQUEST))
        every { defaultTokenExchangeService.exchangeAccessToken(tokenToExchange) } returns exchangedToken.toMono()

        assertThat(underTest.exchangeAccessToken(tokenToExchange).block()).isEqualTo(exchangedToken)

        verify {
            tokenToExchange.subject
            cache[tokenSubject]
            defaultTokenExchangeService.exchangeAccessToken(tokenToExchange)
            tokenExchangeProperties.cache
            cacheProperties.name
            cacheManager.getCache(cacheName)
        }
    }

    @Test
    fun `given invalid token found in cache when token exchange is triggered then token is exchanged and exchange token is stored in cache`() {
        val tokenSubject = "client-1"
        val cacheName = "token-exchange-cache"
        every { tokenToExchange.subject } returns tokenSubject
        every { tokenExchangeProperties.cache } returns cacheProperties
        every { cacheProperties.name } returns cacheName
        every { cacheManager.getCache(cacheName) } returns cache
        every { cache[tokenSubject] } returns ValueWrapper { tokenInCache }
        every { cachedTokenValidator.validate(tokenInCache) } returns failure(OAuth2Error(INVALID_REQUEST))
        every { defaultTokenExchangeService.exchangeAccessToken(tokenToExchange) } returns exchangedToken.toMono()
        every { exchangedToken.expiresAt } returns LocalDateTime.now().toInstant(UTC)
        every { cache.put(tokenSubject, exchangedToken) } returns Unit

        assertThat(underTest.exchangeAccessToken(tokenToExchange).block()).isEqualTo(exchangedToken)

        verify {
            tokenToExchange.subject
            cache[tokenSubject]
            defaultTokenExchangeService.exchangeAccessToken(tokenToExchange)
            exchangedToken.expiresAt
            cache.put(tokenSubject, exchangedToken)
        }
    }

    @Test
    fun `given valid token found in cache when token exchange is triggered then token is not exchanged but only returned`() {
        val tokenSubject = "client-1"
        val cacheName = "token-exchange-cache"
        every { tokenToExchange.subject } returns tokenSubject
        every { tokenExchangeProperties.cache } returns cacheProperties
        every { cacheProperties.name } returns cacheName
        every { cacheManager.getCache(cacheName) } returnsMany listOf(cache, null)
        every { cache[tokenSubject] } returns ValueWrapper { tokenInCache }
        every { cachedTokenValidator.validate(tokenInCache) } returns success()

        assertThat(underTest.exchangeAccessToken(tokenToExchange).block()).isEqualTo(tokenInCache)

        verify {
            tokenToExchange.subject
            cache[tokenSubject]
            cacheManager.getCache(cacheName)
            tokenExchangeProperties.cache
            cacheProperties.name
        }
    }

    @Test
    fun `given valid token found in cache when token exchange is triggered then token is not exchanged nor stored in cache but only returned`() {
        val tokenSubject = "client-1"
        val cacheName = "token-exchange-cache"
        every { tokenToExchange.subject } returns tokenSubject
        every { tokenExchangeProperties.cache } returns cacheProperties
        every { cacheProperties.name } returns cacheName
        every { cacheManager.getCache(cacheName) } returns cache
        every { cache[tokenSubject] } returns ValueWrapper { tokenInCache }
        every { cachedTokenValidator.validate(tokenInCache) } returns success()

        assertThat(underTest.exchangeAccessToken(tokenToExchange).block()).isEqualTo(tokenInCache)

        verify {
            tokenToExchange.subject
            cache[tokenSubject]
            cacheManager.getCache(cacheName)
            tokenExchangeProperties.cache
            cacheProperties.name
        }
    }
}
