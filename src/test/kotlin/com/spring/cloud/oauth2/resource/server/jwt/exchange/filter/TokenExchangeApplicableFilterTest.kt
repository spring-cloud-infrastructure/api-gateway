package com.spring.cloud.oauth2.resource.server.jwt.exchange.filter

import assertk.assertThat
import assertk.assertions.isFalse
import assertk.assertions.isTrue
import com.spring.cloud.config.oauth2.resource.server.jwt.exchange.TokenExchangeProperties
import com.spring.cloud.oauth2.resource.server.jwt.model.ResourceAccessJwt
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.junit.jupiter.api.Test

internal class TokenExchangeApplicableFilterTest {

    private val tokenExchangeProperties = mockk<TokenExchangeProperties>()
    private val token = mockk<ResourceAccessJwt>()

    private val underTest = TokenExchangeApplicableFilter(tokenExchangeProperties)

    @Test
    fun `given token contains resource access claim when exchange applicability is tested then token is not considered as candidate for token exchange`() {
        every { token.containsResourceAccess() } returns true

        assertThat(underTest.test(token)).isFalse()

        verify {
            token.containsResourceAccess()
        }
    }

    @Test
    fun `given token does not contain resource access claim and issued for is not within applicable clients list when exchange applicability is tested then token is not considered as candidate for token exchange`() {
        every { token.containsResourceAccess() } returns false
        every { token.issuedFor } returns "client"
        every { tokenExchangeProperties.applicableClients } returns mutableSetOf("server")

        assertThat(underTest.test(token)).isFalse()

        verify {
            token.containsResourceAccess()
            tokenExchangeProperties.applicableClients
        }
    }

    @Test
    fun `given token does not contain resource access claim and issued for is within applicable clients list when exchange applicability is tested then token is considered as candidate for token exchange`() {
        val issuedFor = "client"
        every { token.containsResourceAccess() } returns false
        every { token.issuedFor } returns issuedFor
        every { tokenExchangeProperties.applicableClients } returns mutableSetOf(issuedFor, "server")

        assertThat(underTest.test(token)).isTrue()

        verify {
            token.containsResourceAccess()
            token.issuedFor
            tokenExchangeProperties.applicableClients
        }
    }
}
