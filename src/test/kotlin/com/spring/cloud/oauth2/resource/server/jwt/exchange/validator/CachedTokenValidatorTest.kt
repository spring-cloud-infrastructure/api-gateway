package com.spring.cloud.oauth2.resource.server.jwt.exchange.validator

import assertk.assertThat
import assertk.assertions.isNotNull
import com.spring.cloud.config.oauth2.resource.server.jwt.exchange.TokenExchangeProperties
import com.spring.cloud.config.oauth2.resource.server.jwt.exchange.TokenExchangeProperties.TokenExchangeCacheProperties
import com.spring.cloud.extension.assertFirstError
import com.spring.cloud.extension.hasAnyErrors
import com.spring.cloud.extension.hasDescriptionEqualTo
import com.spring.cloud.extension.hasErrorCodeEqualTo
import com.spring.cloud.extension.hasErrorsSize
import com.spring.cloud.extension.hasNoErrors
import com.spring.cloud.extension.hasUriEqualTo
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.junit.jupiter.api.Test
import org.springframework.security.oauth2.core.OAuth2ErrorCodes.INVALID_REQUEST
import org.springframework.security.oauth2.jwt.Jwt
import java.time.Clock
import java.time.Duration
import java.time.LocalDateTime
import java.time.ZoneOffset.UTC

internal class CachedTokenValidatorTest {

    private val clock = mockk<Clock>()
    private val tokenExchangeProperties = mockk<TokenExchangeProperties>()
    private val token = mockk<Jwt>()
    private val cache = mockk<TokenExchangeCacheProperties>()

    @Test
    internal fun `given token has minimal JWT expiration time when token validation is triggered then validation ends with success`() {
        every { tokenExchangeProperties.cache } returns cache
        val minimalJwtExpirationTime = Duration.ofDays(3)
        every { cache.minimalJwtExpirationTime } returns minimalJwtExpirationTime
        val currentTime = LocalDateTime.now()
        val expiresAt = currentTime.plus(minimalJwtExpirationTime)
        every { clock.instant() } returns currentTime.toInstant(UTC)
        every { token.expiresAt } returns expiresAt.toInstant(UTC)

        val validatorResult = getUnderTest().validate(token)

        assertThat(validatorResult)
            .isNotNull()
            .hasNoErrors()
        verify {
            tokenExchangeProperties.cache
            cache.minimalJwtExpirationTime
            token.expiresAt
            clock.instant()
        }
    }

    @Test
    internal fun `given token has enough JWT expiration time when token validation is triggered then validation ends with success`() {
        every { tokenExchangeProperties.cache } returns cache
        val minimalJwtExpirationTime = Duration.ofDays(3)
        every { cache.minimalJwtExpirationTime } returns minimalJwtExpirationTime
        val currentTime = LocalDateTime.now()
        val expiresAt = currentTime.plus(minimalJwtExpirationTime).plusSeconds(1)
        every { clock.instant() } returns currentTime.toInstant(UTC)
        every { token.expiresAt } returns expiresAt.toInstant(UTC)

        val validatorResult = getUnderTest().validate(token)

        assertThat(validatorResult)
            .isNotNull()
            .hasNoErrors()
        verify {
            tokenExchangeProperties.cache
            cache.minimalJwtExpirationTime
            token.expiresAt
            clock.instant()
        }
    }

    @Test
    internal fun `given token is not longer valid when token validation is triggered then validation ends with error`() {
        every { tokenExchangeProperties.cache } returns cache
        val minimalJwtExpirationTime = Duration.ofDays(3)
        every { cache.minimalJwtExpirationTime } returns minimalJwtExpirationTime
        val currentTime = LocalDateTime.now()
        val expiresAt = currentTime.plus(minimalJwtExpirationTime).minusSeconds(1)
        every { clock.instant() } returns currentTime.toInstant(UTC)
        every { token.expiresAt } returns expiresAt.toInstant(UTC)

        val validatorResult = getUnderTest().validate(token)

        assertThat(validatorResult)
            .isNotNull()
            .hasAnyErrors()
            .hasErrorsSize(1)
            .assertFirstError()
            .hasErrorCodeEqualTo(INVALID_REQUEST)
            .hasDescriptionEqualTo("Minimal JWT expiration time [$minimalJwtExpirationTime] is not fulfilled, expiration time: [${
                expiresAt.toInstant(UTC)
            }]")
            .hasUriEqualTo("https://tools.ietf.org/html/rfc6750#section-3.1")
        verify {
            tokenExchangeProperties.cache
            cache.minimalJwtExpirationTime
            clock.instant()
            token.expiresAt
        }
    }

    @Test
    internal fun `given token has no expiry defined when token validation is triggered then validation ends with error`() {
        val minimalJwtExpirationTime = Duration.ofDays(3)
        every { tokenExchangeProperties.cache } returns cache
        every { cache.minimalJwtExpirationTime } returns minimalJwtExpirationTime
        every { token.expiresAt } returns null

        val validatorResult = getUnderTest().validate(token)

        assertThat(validatorResult)
            .isNotNull()
            .hasAnyErrors()
            .hasErrorsSize(1)
            .assertFirstError()
            .hasErrorCodeEqualTo(INVALID_REQUEST)
            .hasDescriptionEqualTo("Minimal JWT expiration time [$minimalJwtExpirationTime] is not fulfilled, expiration time: [null]")
            .hasUriEqualTo("https://tools.ietf.org/html/rfc6750#section-3.1")
        verify {
            tokenExchangeProperties.cache
            cache.minimalJwtExpirationTime
            token.expiresAt
        }
    }

    private fun getUnderTest() = CachedTokenValidator(clock, tokenExchangeProperties)
}
