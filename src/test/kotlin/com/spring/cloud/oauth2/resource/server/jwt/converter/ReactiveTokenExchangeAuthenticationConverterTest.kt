package com.spring.cloud.oauth2.resource.server.jwt.converter

import assertk.assertThat
import assertk.assertions.isEqualTo
import assertk.assertions.isNotNull
import com.spring.cloud.extension.assertToken
import com.spring.cloud.extension.hasAuthoritiesEqualTo
import com.spring.cloud.extension.hasClaimsEqualTo
import com.spring.cloud.extension.hasExpiresAtEqualTo
import com.spring.cloud.extension.hasHeadersEqualTo
import com.spring.cloud.extension.hasIssuedAtEqualTo
import com.spring.cloud.extension.hasTokenEqualTo
import com.spring.cloud.oauth2.resource.server.jwt.exchange.TokenExchangeService
import com.spring.cloud.oauth2.resource.server.jwt.exchange.filter.TokenExchangeApplicableFilter
import com.spring.cloud.oauth2.resource.server.jwt.model.JwtGrantedAuthority
import com.spring.cloud.oauth2.resource.server.jwt.model.ResourceAccessJwt
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.junit.jupiter.api.Test
import org.springframework.security.oauth2.jwt.Jwt
import org.springframework.security.oauth2.jwt.JwtClaimNames.AUD
import org.springframework.security.oauth2.jwt.JwtClaimNames.ISS
import org.springframework.security.oauth2.jwt.JwtClaimNames.NBF
import org.springframework.security.oauth2.jwt.JwtClaimNames.SUB
import reactor.kotlin.core.publisher.toMono
import java.time.LocalDateTime
import java.time.ZoneOffset.UTC

@ExperimentalStdlibApi
internal class ReactiveTokenExchangeAuthenticationConverterTest {

    private val tokenExchangeApplicableFilter = mockk<TokenExchangeApplicableFilter>()
    private val cachedTokenExchangeService = mockk<TokenExchangeService>()
    private val jwtGrantedAuthorityConverter = mockk<ReactiveJwtGrantedAuthorityConverter>()

    private val underTest = ReactiveTokenExchangeAuthenticationConverter(
        tokenExchangeApplicableFilter,
        cachedTokenExchangeService,
        jwtGrantedAuthorityConverter
    )

    companion object {
        private const val JWT_ACCESS_TOKEN_1 =
            "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJKeUVlaGdoenRIZk5wQzhHRG9oY1V0bGlTREJhOUtlaHRPalI2eDRGZmxFIn0.eyJleHAiOjE2MTUwNjk4NzMsImlzcyI6Imh0dHBzOi8vMTAuMC4wLjE6ODAvYXV0aC9yZWFsbXMvZGV2Iiwic3ViIjoiYjIyODc2ZjUtOTdiYS00MDRhLWJjMTctNjQwMjk0N2NkZGFiIiwidHlwIjoiQmVhcmVyIiwiYXpwIjoidGVzdC1jbGllbnQiLCJzZXNzaW9uX3N0YXRlIjoiZjA0ZTA3YzUtZmQ2My00YmZjLWFlMGItZWM5MDU5MmU2ZDY1In0.PPV5b-RQyqzdZvjKVHPkgPBIwLBqS-09YMg-wKZA1Lzo1givqboEE95U8qNLR9AM69wJsIHUem7vul6nBwPWRO5Cb7_Jo68G0gGs2XrlhzgTFVkbcYlJhkdW_IGRxGDTlM7dhUVezRAY7skTcX6EHVw9EM1FMCkpqwHUb-JHUb2ftUhoVhUzg7bWzkaUE_Uj_a9AlPFg8Paw1L1SL2lg7BD6gr51EWSdkhLafCt0z_PY8-Cs4wWRfc52m72TIZgwwLeSnKsvY6hDr5uhN0RF3k65ylHp-Io0UUhTTC3FHGLXAiHCz8AxBXQmoT7KknBPnupqoMG0Aa8MnmdGSAiuKQ"
        private const val JWT_ACCESS_TOKEN_2 =
            "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c"
    }

    @Test
    fun `give token exchange is not applicable when JWT is converted to JWT authentication token then converted JWT authentication token is returned`() {
        val issuedAt = LocalDateTime.MIN.toInstant(UTC)
        val expiresAt = LocalDateTime.MAX.toInstant(UTC)
        val headers = buildMap<String, String> {
            put(SUB, "a885feb9-63a5-4d60-9466-cfc7d346d173")
        }
        val claims = buildMap<String, String> {
            put(ISS, "server")
        }
        val jwt = Jwt(JWT_ACCESS_TOKEN_1, issuedAt, expiresAt, headers, claims)
        every { tokenExchangeApplicableFilter.test(any()) } returns false
        val grantedAuthorities = buildSet {
            add(JwtGrantedAuthority("role1"))
            add(JwtGrantedAuthority("role2"))
            add(JwtGrantedAuthority("role3"))
            add(JwtGrantedAuthority("role4"))
        }
        every { jwtGrantedAuthorityConverter.convert(any()) } returns grantedAuthorities.toMono()

        val jwtAuthenticationToken = underTest.convert(jwt).block()

        assertThat(jwtAuthenticationToken)
            .isNotNull()
            .hasAuthoritiesEqualTo(grantedAuthorities)
            .assertToken()
            .isNotNull()
            .hasTokenEqualTo(JWT_ACCESS_TOKEN_1)
            .hasIssuedAtEqualTo(issuedAt)
            .hasExpiresAtEqualTo(expiresAt)
            .hasHeadersEqualTo(headers)
            .hasClaimsEqualTo(claims)
        verify {
            tokenExchangeApplicableFilter.test(withArg {
                assertThat(it)
                    .hasTokenEqualTo(JWT_ACCESS_TOKEN_1)
                    .hasIssuedAtEqualTo(issuedAt)
                    .hasExpiresAtEqualTo(expiresAt)
                    .hasHeadersEqualTo(headers)
                    .hasClaimsEqualTo(claims)
            })
            jwtGrantedAuthorityConverter.convert(withArg {
                assertThat(it)
                    .hasTokenEqualTo(JWT_ACCESS_TOKEN_1)
                    .hasIssuedAtEqualTo(issuedAt)
                    .hasExpiresAtEqualTo(expiresAt)
                    .hasHeadersEqualTo(headers)
                    .hasClaimsEqualTo(claims)
            })
        }
    }

    @Test
    fun `give token exchange is applicable when JWT is converted to JWT authentication token then exchanged and converted JWT authentication token is returned`() {
        val originalIssuedAt = LocalDateTime.MIN.toInstant(UTC)
        val originalExpiresAt = LocalDateTime.MAX.toInstant(UTC)
        val originalHeaders = buildMap<String, String> {
            put(SUB, "a885feb9-63a5-4d60-9466-cfc7d346d173")
        }
        val originalClaims = buildMap<String, String> {
            put(ISS, "server")
        }
        val originalJwt = Jwt(JWT_ACCESS_TOKEN_1, originalIssuedAt, originalExpiresAt, originalHeaders, originalClaims)
        every { tokenExchangeApplicableFilter.test(any()) } returns true
        val newIssuedAt = LocalDateTime.MIN.plusDays(1).toInstant(UTC)
        val newExpiresAt = LocalDateTime.MAX.minusDays(1).toInstant(UTC)
        val newHeaders = buildMap<String, String> {
            put(AUD, "service-discovery")
        }
        val newClaims = buildMap<String, String> {
            put(NBF, "authorization-server")
        }
        val newJwt = Jwt(JWT_ACCESS_TOKEN_2, newIssuedAt, newExpiresAt, newHeaders, newClaims)
        val newResourceAccessJwt = ResourceAccessJwt(newJwt)
        every { cachedTokenExchangeService.exchangeAccessToken(any()) } returns newResourceAccessJwt.toMono()
        val grantedAuthorities = buildSet {
            add(JwtGrantedAuthority("role1"))
            add(JwtGrantedAuthority("role2"))
            add(JwtGrantedAuthority("role3"))
            add(JwtGrantedAuthority("role4"))
        }
        every { jwtGrantedAuthorityConverter.convert(any()) } returns grantedAuthorities.toMono()

        val jwtAuthenticationToken = underTest.convert(originalJwt).block()

        assertThat(jwtAuthenticationToken)
            .isNotNull()
            .hasAuthoritiesEqualTo(grantedAuthorities)
            .assertToken()
            .isEqualTo(newResourceAccessJwt)
        verify {
            tokenExchangeApplicableFilter.test(withArg {
                assertThat(it)
                    .hasTokenEqualTo(JWT_ACCESS_TOKEN_1)
                    .hasIssuedAtEqualTo(originalIssuedAt)
                    .hasExpiresAtEqualTo(originalExpiresAt)
                    .hasHeadersEqualTo(originalHeaders)
                    .hasClaimsEqualTo(originalClaims)
            })
            cachedTokenExchangeService.exchangeAccessToken(withArg {
                assertThat(it)
                    .hasTokenEqualTo(JWT_ACCESS_TOKEN_1)
                    .hasIssuedAtEqualTo(originalIssuedAt)
                    .hasExpiresAtEqualTo(originalExpiresAt)
                    .hasHeadersEqualTo(originalHeaders)
                    .hasClaimsEqualTo(originalClaims)
            })
            jwtGrantedAuthorityConverter.convert(withArg {
                assertThat(it)
                    .hasTokenEqualTo(JWT_ACCESS_TOKEN_2)
                    .hasIssuedAtEqualTo(newIssuedAt)
                    .hasExpiresAtEqualTo(newExpiresAt)
                    .hasHeadersEqualTo(newHeaders)
                    .hasClaimsEqualTo(newClaims)
            })
        }
    }
}
