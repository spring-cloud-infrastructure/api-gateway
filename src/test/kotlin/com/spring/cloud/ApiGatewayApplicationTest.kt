package com.spring.cloud

import org.junit.jupiter.api.Test
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
internal class ApiGatewayApplicationTest {

    @Test
    fun contextLoads() {
        // this method is empty purposely regarding of testing of application context
    }
}
