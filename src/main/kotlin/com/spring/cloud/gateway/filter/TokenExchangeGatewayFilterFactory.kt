package com.spring.cloud.gateway.filter

import mu.KotlinLogging.logger
import org.springframework.cloud.gateway.filter.GatewayFilter
import org.springframework.cloud.gateway.filter.GatewayFilterChain
import org.springframework.cloud.gateway.filter.factory.AbstractGatewayFilterFactory
import org.springframework.cloud.gateway.support.GatewayToStringStyler.filterToStringCreator
import org.springframework.http.HttpHeaders.AUTHORIZATION
import org.springframework.security.oauth2.jwt.Jwt
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken
import org.springframework.stereotype.Component
import org.springframework.web.server.ServerWebExchange
import reactor.core.publisher.Mono

/**
 * This gateway filter gets the [JwtAuthenticationToken] from [ServerWebExchange], extracts [Jwt] from
 * there and sets it to [ServerWebExchange] authorization header. Overriding of authorization header is required
 * due to logic implemented in [ReactiveTokenExchangeAuthenticationConverter]
 */
@Component
internal class TokenExchangeGatewayFilterFactory : AbstractGatewayFilterFactory<TokenExchangeGatewayFilterConfig>(
    TokenExchangeGatewayFilterConfig::class.java
) {

    private val logger = logger { }

    companion object {
        private const val BEARER_TOKEN_PREFIX = "Bearer "
    }

    override fun apply(config: TokenExchangeGatewayFilterConfig): GatewayFilter {
        return object : GatewayFilter {

            override fun filter(exchange: ServerWebExchange, chain: GatewayFilterChain): Mono<Void> =
                exchange.getPrincipal<JwtAuthenticationToken>()
                    .map(JwtAuthenticationToken::getToken)
                    .map(Jwt::getTokenValue)
                    .map { setAuthorizationBearerToken(exchange, it) }
                    .defaultIfEmpty(exchange.also {
                        logger.warn { "Skipping token exchange due to missing JWT authentication token principal in request" }
                    })
                    .flatMap(chain::filter)

            override fun toString(): String = "${filterToStringCreator(this@TokenExchangeGatewayFilterFactory)}"

            private fun setAuthorizationBearerToken(
                exchange: ServerWebExchange,
                token: String,
            ): ServerWebExchange = exchange.mutate()
                .request { request -> request.header(AUTHORIZATION, "$BEARER_TOKEN_PREFIX$token") }
                .build()
        }
    }
}
