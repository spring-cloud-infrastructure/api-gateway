package com.spring.cloud.problem

import org.springframework.security.oauth2.core.OAuth2TokenValidatorResult
import org.zalando.problem.AbstractThrowableProblem
import org.zalando.problem.Exceptional
import org.zalando.problem.Status.UNAUTHORIZED

class TokenToExchangeValidationProblem(validatorResult: OAuth2TokenValidatorResult) : AbstractThrowableProblem(
    null,
    "Token Validation Error",
    UNAUTHORIZED,
    "JWT token has the following validation errors: ${validatorResult.errors}"
) {

    override fun getCause(): Exceptional? = super.cause
}
