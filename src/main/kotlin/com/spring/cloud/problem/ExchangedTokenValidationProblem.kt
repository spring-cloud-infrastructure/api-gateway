package com.spring.cloud.problem

import com.nimbusds.oauth2.sdk.TokenErrorResponse
import org.zalando.problem.AbstractThrowableProblem
import org.zalando.problem.Exceptional
import org.zalando.problem.Status.INTERNAL_SERVER_ERROR

class ExchangedTokenValidationProblem(tokenErrorResponse: TokenErrorResponse) : AbstractThrowableProblem(
    null,
    "Token Validation Error",
    INTERNAL_SERVER_ERROR,
    "Exchanged JWT token is invalid due to following validation errors: ${tokenErrorResponse.errorObject}"
) {

    override fun getCause(): Exceptional? = super.cause
}
