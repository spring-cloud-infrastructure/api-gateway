package com.spring.cloud.problem

import org.zalando.problem.AbstractThrowableProblem
import org.zalando.problem.Exceptional
import org.zalando.problem.Status.INTERNAL_SERVER_ERROR

class UnparseableExchangedTokenProblem : AbstractThrowableProblem(
    null,
    "Token Parsing Error",
    INTERNAL_SERVER_ERROR,
    "Exchanged token response cannot be parsed to TokenResponse"
) {

    override fun getCause(): Exceptional? = super.cause
}
