package com.spring.cloud.problem.handler

import org.springframework.web.bind.annotation.ControllerAdvice
import org.zalando.problem.spring.webflux.advice.ProblemHandling
import org.zalando.problem.spring.webflux.advice.security.SecurityAdviceTrait

@ControllerAdvice
internal class ProblemHandler : ProblemHandling, SecurityAdviceTrait
