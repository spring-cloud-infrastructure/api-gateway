package com.spring.cloud.problem

import org.zalando.problem.AbstractThrowableProblem
import org.zalando.problem.Exceptional
import org.zalando.problem.Status.INTERNAL_SERVER_ERROR

class UnparseableTokenProblem : AbstractThrowableProblem(
    null,
    "Unparseable Token Error",
    INTERNAL_SERVER_ERROR,
    "JWT token cannot be parsed"
) {

    override fun getCause(): Exceptional? = super.cause
}
