package com.spring.cloud.problem

import org.zalando.problem.AbstractThrowableProblem
import org.zalando.problem.Exceptional
import org.zalando.problem.Status.INTERNAL_SERVER_ERROR

class NoSuitableDecoderFoundProblem(issuerUri: String) : AbstractThrowableProblem(
    null,
    "No Suitable JWT Decoder Found Error",
    INTERNAL_SERVER_ERROR,
    "No suitable JWT decoder found for issuer: [$issuerUri]"
) {

    override fun getCause(): Exceptional? = super.cause
}
