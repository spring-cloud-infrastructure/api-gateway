package com.spring.cloud

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.web.reactive.error.ErrorWebFluxAutoConfiguration
import org.springframework.boot.context.properties.ConfigurationPropertiesScan
import org.springframework.boot.runApplication
import org.springframework.retry.annotation.EnableRetry
import reactivefeign.spring.config.EnableReactiveFeignClients

@SpringBootApplication(exclude = [
    ErrorWebFluxAutoConfiguration::class
])
@EnableReactiveFeignClients
@EnableRetry
@ConfigurationPropertiesScan
class ApiGatewayApplication

fun main(args: Array<String>) {
    runApplication<ApiGatewayApplication>(*args)
}
