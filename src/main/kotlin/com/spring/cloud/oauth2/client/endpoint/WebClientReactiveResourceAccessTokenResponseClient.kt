package com.spring.cloud.oauth2.client.endpoint

import com.spring.cloud.oauth2.resource.server.jwt.converter.ReactiveJwtGrantedAuthorityConverter
import com.spring.cloud.oauth2.resource.server.jwt.model.JwtGrantedAuthority
import com.spring.cloud.oauth2.resource.server.jwt.model.ResourceAccessJwt
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.security.oauth2.client.endpoint.OAuth2AuthorizationCodeGrantRequest
import org.springframework.security.oauth2.client.endpoint.ReactiveOAuth2AccessTokenResponseClient
import org.springframework.security.oauth2.core.ClientAuthenticationMethod.BASIC
import org.springframework.security.oauth2.core.ClientAuthenticationMethod.POST
import org.springframework.security.oauth2.core.OAuth2AccessToken
import org.springframework.security.oauth2.core.endpoint.OAuth2AccessTokenResponse
import org.springframework.security.oauth2.core.endpoint.OAuth2ParameterNames.CLIENT_ID
import org.springframework.security.oauth2.core.endpoint.OAuth2ParameterNames.CLIENT_SECRET
import org.springframework.security.oauth2.core.endpoint.OAuth2ParameterNames.CODE
import org.springframework.security.oauth2.core.endpoint.OAuth2ParameterNames.GRANT_TYPE
import org.springframework.security.oauth2.core.endpoint.OAuth2ParameterNames.REDIRECT_URI
import org.springframework.security.oauth2.core.endpoint.PkceParameterNames.CODE_VERIFIER
import org.springframework.security.oauth2.core.web.reactive.function.OAuth2BodyExtractors
import org.springframework.security.oauth2.jwt.ReactiveJwtDecoder
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.BodyInserters
import org.springframework.web.reactive.function.client.ClientResponse
import org.springframework.web.reactive.function.client.WebClient
import reactor.core.publisher.Mono
import reactor.kotlin.core.publisher.toFlux
import reactor.kotlin.core.publisher.toMono
import java.util.stream.Collectors.toSet

/**
 * Slightly changed implementation of [WebClientReactiveAuthorizationCodeTokenResponseClient] which takes client's
 * granted authorities from a custom JWT claim
 *
 * @see ReactiveJwtGrantedAuthorityConverter
 */
@Component
internal class WebClientReactiveResourceAccessTokenResponseClient(
    private val jwtGrantedAuthorityConverter: ReactiveJwtGrantedAuthorityConverter,
    private val jwtDecoder: ReactiveJwtDecoder,
    private val webClient: WebClient,
) : ReactiveOAuth2AccessTokenResponseClient<OAuth2AuthorizationCodeGrantRequest> {

    override fun getTokenResponse(grantRequest: OAuth2AuthorizationCodeGrantRequest): Mono<OAuth2AccessTokenResponse> =
        Mono.defer {
            webClient.post()
                .uri(getTokenUri(grantRequest))
                .headers { populateTokenRequestHeaders(grantRequest, it) }
                .body(createTokenRequestBody(grantRequest))
                .exchange()
                .flatMap(::readTokenResponse)
        }

    private fun getTokenUri(grantRequest: OAuth2AuthorizationCodeGrantRequest): String =
        grantRequest.clientRegistration.providerDetails.tokenUri

    private fun populateTokenRequestHeaders(
        grantRequest: OAuth2AuthorizationCodeGrantRequest,
        headers: HttpHeaders,
    ) {
        val clientRegistration = grantRequest.clientRegistration
        headers.contentType = MediaType.APPLICATION_FORM_URLENCODED
        headers.accept = listOf(MediaType.APPLICATION_JSON)
        if (BASIC == clientRegistration.clientAuthenticationMethod) {
            headers.setBasicAuth(clientRegistration.clientId, clientRegistration.clientSecret)
        }
    }

    private fun createTokenRequestBody(grantRequest: OAuth2AuthorizationCodeGrantRequest): BodyInserters.FormInserter<String> {
        val body = BodyInserters.fromFormData(GRANT_TYPE, grantRequest.grantType.value)
        val clientRegistration = grantRequest.clientRegistration
        if (BASIC != clientRegistration.clientAuthenticationMethod) {
            body.with(CLIENT_ID, clientRegistration.clientId)
        }
        if (POST == clientRegistration.clientAuthenticationMethod) {
            body.with(CLIENT_SECRET, clientRegistration.clientSecret)
        }
        val authorizationExchange = grantRequest.authorizationExchange
        val authorizationResponse = authorizationExchange.authorizationResponse
        body.with(CODE, authorizationResponse.code)
        val redirectUri = authorizationExchange.authorizationRequest.redirectUri
        if (redirectUri != null) {
            body.with(REDIRECT_URI, redirectUri)
        }
        val codeVerifier = authorizationExchange.authorizationRequest.getAttribute<String>(CODE_VERIFIER)
        if (codeVerifier != null) {
            body.with(CODE_VERIFIER, codeVerifier)
        }
        return body
    }

    private fun readTokenResponse(response: ClientResponse): Mono<OAuth2AccessTokenResponse> =
        response.body(OAuth2BodyExtractors.oauth2AccessTokenResponse())
            .flatMap(::populateTokenResponse)

    private fun populateTokenResponse(tokenResponse: OAuth2AccessTokenResponse): Mono<OAuth2AccessTokenResponse> =
        tokenResponse.toMono()
            .map(OAuth2AccessTokenResponse::getAccessToken)
            .map(OAuth2AccessToken::getTokenValue)
            .flatMap(jwtDecoder::decode)
            .map(::ResourceAccessJwt)
            .flatMap(jwtGrantedAuthorityConverter::convert)
            .flatMapMany(Set<JwtGrantedAuthority>::toFlux)
            .map(JwtGrantedAuthority::authorityWithNoScopePrefix)
            .collect(toSet())
            .defaultIfEmpty(emptySet())
            .map { OAuth2AccessTokenResponse.withResponse(tokenResponse).scopes(it).build() }
}
