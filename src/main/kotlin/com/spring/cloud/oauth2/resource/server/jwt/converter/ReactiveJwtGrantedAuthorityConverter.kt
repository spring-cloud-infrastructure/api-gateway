package com.spring.cloud.oauth2.resource.server.jwt.converter

import com.spring.cloud.oauth2.resource.server.jwt.model.JwtGrantedAuthority
import com.spring.cloud.oauth2.resource.server.jwt.model.ResourceAccess
import com.spring.cloud.oauth2.resource.server.jwt.model.ResourceAccessJwt
import org.apache.commons.lang3.StringUtils
import org.springframework.core.convert.converter.Converter
import org.springframework.stereotype.Component
import reactor.core.publisher.Mono
import reactor.kotlin.core.publisher.toFlux
import reactor.kotlin.core.publisher.toMono
import java.util.stream.Collectors.toSet

/**
 * Extracts list of [JwtGrantedAuthority] from resource access claim of provided [ResourceAccessJwt] by
 * applying the following formula:
 *
 *
 * `granted authority = uppercase(resource name) + `[ReactiveJwtGrantedAuthorityConverter.RESOURCE_ROLE_SEPARATOR]
 * `+ uppercase(role)`
 */
@Component
class ReactiveJwtGrantedAuthorityConverter : Converter<ResourceAccessJwt, Mono<Set<JwtGrantedAuthority>>> {

    companion object {
        private const val RESOURCE_ROLE_SEPARATOR = ":"
    }

    override fun convert(token: ResourceAccessJwt): Mono<Set<JwtGrantedAuthority>> = token.toMono()
        .map(ResourceAccessJwt::resourceAccess)
        .flatMapMany(Set<ResourceAccess>::toFlux)
        .flatMap(::getGrantedAuthoritiesForResource)
        .flatMap(Set<JwtGrantedAuthority>::toFlux)
        .collect(toSet())
        .defaultIfEmpty(emptySet())

    private fun getGrantedAuthoritiesForResource(resourceAccess: ResourceAccess): Mono<Set<JwtGrantedAuthority>> =
        resourceAccess.toMono()
            .map(ResourceAccess::roles)
            .flatMapMany(Set<String>::toFlux)
            .filter(StringUtils::isNotBlank)
            .map { "${resourceAccess.resourceName}$RESOURCE_ROLE_SEPARATOR$it" }
            .map(StringUtils::upperCase)
            .distinct()
            .map(::JwtGrantedAuthority)
            .collect(toSet())
}
