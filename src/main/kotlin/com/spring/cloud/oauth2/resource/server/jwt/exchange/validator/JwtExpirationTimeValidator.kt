package com.spring.cloud.oauth2.resource.server.jwt.exchange.validator

import org.springframework.security.oauth2.core.OAuth2Error
import org.springframework.security.oauth2.core.OAuth2ErrorCodes.INVALID_REQUEST
import org.springframework.security.oauth2.core.OAuth2TokenValidator
import org.springframework.security.oauth2.core.OAuth2TokenValidatorResult
import org.springframework.security.oauth2.core.OAuth2TokenValidatorResult.failure
import org.springframework.security.oauth2.core.OAuth2TokenValidatorResult.success
import org.springframework.security.oauth2.jwt.Jwt
import java.time.Clock
import java.time.Duration
import java.time.Instant

class JwtExpirationTimeValidator(
    private val clock: Clock,
    private val minimalExpirationTime: Duration,
) : OAuth2TokenValidator<Jwt> {

    override fun validate(token: Jwt): OAuth2TokenValidatorResult {
        return when {
            token.expiresAt == null || isExpired(token) -> failure(createTokenExpirationError(token))
            else -> success()
        }
    }

    private fun createTokenExpirationError(token: Jwt): OAuth2Error = OAuth2Error(
        INVALID_REQUEST,
        "Minimal JWT expiration time [$minimalExpirationTime] is not fulfilled, expiration time: [${token.expiresAt}]",
        "https://tools.ietf.org/html/rfc6750#section-3.1")

    private fun isExpired(token: Jwt) = Instant.now(clock).plus(minimalExpirationTime).isAfter(token.expiresAt)
}
