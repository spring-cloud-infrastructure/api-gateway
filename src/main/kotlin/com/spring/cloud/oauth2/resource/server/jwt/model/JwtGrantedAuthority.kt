package com.spring.cloud.oauth2.resource.server.jwt.model

import org.apache.commons.lang3.StringUtils.prependIfMissing
import org.springframework.security.core.GrantedAuthority

/**
 * Extends [SimpleGrantedAuthority] with [JwtGrantedAuthority.SCOPE_GRANTED_AUTHORITY_PREFIX] support
 * because calls made from UI generate authorities with [JwtGrantedAuthority.SCOPE_GRANTED_AUTHORITY_PREFIX]
 * prefix in [OidcReactiveOAuth2UserService.loadUser], while calls made by API do not. Though we shall keep the
 * same logic for both of them to support roles based spring security, e.g. in [SecurityWebFilterChain] bean.
 */
data class JwtGrantedAuthority(val authorityWithNoScopePrefix: String) : GrantedAuthority {

    companion object {
        private const val SCOPE_GRANTED_AUTHORITY_PREFIX = "SCOPE_"
    }

    override fun getAuthority(): String {
        return prependIfMissing(authorityWithNoScopePrefix, SCOPE_GRANTED_AUTHORITY_PREFIX)
    }
}
