package com.spring.cloud.oauth2.resource.server.jwt.model

import com.google.common.base.Splitter
import org.springframework.core.convert.TypeDescriptor
import org.springframework.security.oauth2.core.converter.ClaimConversionService
import org.springframework.security.oauth2.core.oidc.IdTokenClaimNames.AZP
import org.springframework.security.oauth2.jwt.Jwt
import java.net.URL

class ResourceAccessJwt(token: Jwt) : Jwt(
    token.tokenValue,
    token.issuedAt,
    token.expiresAt,
    token.headers,
    token.claims
) {

    companion object {
        private const val ROLES_FIELD = "roles"
        private const val RESOURCE_ACCESS_CLAIM = "resource_access"
        private val URI_SPLITTER = Splitter.on("/")
    }

    val issuedFor: String?
        get() = getClaimAsString(AZP)

    val realmName: String?
        get() {
            val issuerPaths = issuer?.let(URL::getPath)?.let(URI_SPLITTER::splitToList)
            return when {
                issuerPaths?.size!! > 1 -> issuerPaths[issuerPaths.size - 1]
                else -> null
            }
        }

    val resourceAccess: Set<ResourceAccess>
        get() {
            return getRawResourceAccess().entries
                .filter { it.value.containsKey(ROLES_FIELD) }
                .map {
                    ResourceAccess(
                        resourceName = it.key,
                        roles = it.value[ROLES_FIELD]!!.toSet()
                    )
                }
                .toSet()
        }

    private fun getRawResourceAccess(): Map<String, Map<String, List<String>>> {
        if (!containsResourceAccess()) {
            return emptyMap()
        }

        val rolesListTypeDescriptor = TypeDescriptor.collection(
            MutableList::class.java,
            TypeDescriptor.valueOf(String::class.java)
        )
        val rolesWrapperTypeDescriptor = TypeDescriptor.map(
            MutableMap::class.java,
            TypeDescriptor.valueOf(String::class.java),
            rolesListTypeDescriptor
        )
        val resourceAccessTypeDescriptor = TypeDescriptor.map(
            MutableMap::class.java,
            TypeDescriptor.valueOf(String::class.java),
            rolesWrapperTypeDescriptor
        )
        val sourceDescriptor = TypeDescriptor.valueOf(
            Any::class.java
        )
        return ClaimConversionService.getSharedInstance().convert(
            claims[RESOURCE_ACCESS_CLAIM],
            sourceDescriptor,
            resourceAccessTypeDescriptor
        ) as Map<String, Map<String, List<String>>>?
            ?: emptyMap()
    }

    fun containsResourceAccess(): Boolean {
        return containsClaim(RESOURCE_ACCESS_CLAIM)
    }
}
