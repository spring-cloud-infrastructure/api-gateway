package com.spring.cloud.oauth2.resource.server.jwt.exchange

import com.nimbusds.oauth2.sdk.AccessTokenResponse
import com.nimbusds.oauth2.sdk.ParseException
import com.nimbusds.oauth2.sdk.TokenResponse
import com.nimbusds.oauth2.sdk.token.AccessToken
import com.nimbusds.oauth2.sdk.token.Tokens
import com.spring.cloud.oauth2.resource.server.jwt.exchange.validator.TokenToExchangeValidator
import com.spring.cloud.oauth2.resource.server.jwt.model.ResourceAccessJwt
import com.spring.cloud.oauth2.resource.server.jwt.provider.AuthorizationServerApiClient
import com.spring.cloud.oauth2.resource.server.jwt.provider.model.TokenExchangeRequest
import com.spring.cloud.problem.ExchangedTokenValidationProblem
import com.spring.cloud.problem.TokenToExchangeValidationProblem
import com.spring.cloud.problem.UnparseableExchangedTokenProblem
import mu.KotlinLogging.logger
import net.minidev.json.JSONObject
import org.springframework.security.oauth2.client.registration.ClientRegistration
import org.springframework.security.oauth2.jwt.ReactiveJwtDecoder
import org.springframework.stereotype.Service
import reactor.core.publisher.Mono
import reactor.kotlin.core.publisher.toMono

@Service
internal class DefaultTokenExchangeService(
    private val tokenToExchangeValidator: TokenToExchangeValidator,
    private val authorizationServerApiClient: AuthorizationServerApiClient,
    private val clientRegistration: ClientRegistration,
    private val jwtDecoder: ReactiveJwtDecoder,
) : TokenExchangeService {

    private val logger = logger { }

    override fun exchangeAccessToken(token: ResourceAccessJwt): Mono<ResourceAccessJwt> {
        return token.toMono()
            .filter(::isTokenToExchangeValid)
            .map(::buildTokenExchangeRequest)
            .flatMap {
                authorizationServerApiClient.exchangeToken(
                    "Bearer ${it.sourceToken}",
                    token.realmName!!,
                    it
                )
            }
            .map(::mapToTokenResponse)
            .filter(::isExchangedTokenValid)
            .map(TokenResponse::toSuccessResponse)
            .map(AccessTokenResponse::getTokens)
            .map(Tokens::getAccessToken) // TODO: implement refresh token flow for cases when a token in cache is expired
            .map(AccessToken::getValue)
            .flatMap(jwtDecoder::decode)
            .map(::ResourceAccessJwt)
    }

    private fun isTokenToExchangeValid(token: ResourceAccessJwt): Boolean {
        val validatorResult = tokenToExchangeValidator.validate(token)
        if (validatorResult.hasErrors()) {
            throw TokenToExchangeValidationProblem(validatorResult)
        }
        return true
    }

    private fun buildTokenExchangeRequest(token: ResourceAccessJwt): TokenExchangeRequest = TokenExchangeRequest(
        sourceClientId = token.issuedFor!!,
        sourceToken = token.tokenValue,
        targetClientId = clientRegistration.clientId
    )

    private fun mapToTokenResponse(response: JSONObject): TokenResponse = try {
        TokenResponse.parse(response)

    } catch (e: ParseException) {
        logger.error(e) { "An error has occurred while parsing an exchanged JWT token" }
        throw UnparseableExchangedTokenProblem()
    }

    private fun isExchangedTokenValid(tokenResponse: TokenResponse): Boolean = when {
        tokenResponse.indicatesSuccess() -> true
        else -> throw ExchangedTokenValidationProblem(tokenResponse.toErrorResponse())
    }
}
