package com.spring.cloud.oauth2.resource.server.jwt.exchange.filter

import com.spring.cloud.config.oauth2.resource.server.jwt.exchange.TokenExchangeProperties
import com.spring.cloud.oauth2.resource.server.jwt.model.ResourceAccessJwt
import mu.KotlinLogging.logger
import org.springframework.stereotype.Component
import java.util.function.Predicate

@Component
class TokenExchangeApplicableFilter(
    private val tokenExchangeProperties: TokenExchangeProperties,
) : Predicate<ResourceAccessJwt> {

    private val logger = logger { }

    override fun test(token: ResourceAccessJwt): Boolean {
        return when {
            token.containsResourceAccess() -> {
                logger.debug { "Token exchange is not applicable because an existing resource access claim found within the original JWT" }
                false
            }

            token.issuedFor !in tokenExchangeProperties.applicableClients -> {
                logger.debug { "Token exchange is not applicable because a non-applicable client [${token.issuedFor}] found in the original JWT" }
                false
            }

            else -> true
        }
    }
}
