package com.spring.cloud.oauth2.resource.server.jwt.provider.model

import org.springframework.util.LinkedMultiValueMap

data class TokenExchangeRequest(
    val sourceClientId: String,
    val sourceToken: String,
    val targetClientId: String,
) : LinkedMultiValueMap<String, String>() {

    companion object {
        private const val GRANT_TYPE_FIELD_NAME = "grant_type"
        private const val CLIENT_ID_FIELD_NAME = "client_id"
        private const val SUBJECT_TOKEN_FIELD_NAME = "subject_token"
        private const val SUBJECT_TOKEN_TYPE_FIELD_NAME = "subject_token_type"
        private const val AUDIENCE_FIELD_NAME = "audience"
        private const val REQUESTED_TOKEN_TYPE_FIELD_NAME = "requested_token_type"
        private const val TOKEN_EXCHANGE_GRANT_TYPE = "urn:ietf:params:oauth:grant-type:token-exchange"
        private const val ACCESS_TOKEN_TOKEN_TYPE = "urn:ietf:params:oauth:token-type:access_token"
    }

    init {
        set(GRANT_TYPE_FIELD_NAME, TOKEN_EXCHANGE_GRANT_TYPE)
        set(CLIENT_ID_FIELD_NAME, sourceClientId)
        set(SUBJECT_TOKEN_FIELD_NAME, sourceToken)
        set(SUBJECT_TOKEN_TYPE_FIELD_NAME, ACCESS_TOKEN_TOKEN_TYPE)
        set(AUDIENCE_FIELD_NAME, targetClientId)
        set(REQUESTED_TOKEN_TYPE_FIELD_NAME, ACCESS_TOKEN_TOKEN_TYPE)
    }
}
