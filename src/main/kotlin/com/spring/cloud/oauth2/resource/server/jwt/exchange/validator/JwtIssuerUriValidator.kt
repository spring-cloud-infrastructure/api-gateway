package com.spring.cloud.oauth2.resource.server.jwt.exchange.validator

import com.spring.cloud.config.oauth2.resource.server.jwt.decoder.MultiTenantOauth2ResourceServerProperties
import org.springframework.boot.autoconfigure.security.oauth2.resource.OAuth2ResourceServerProperties
import org.springframework.security.oauth2.core.OAuth2Error
import org.springframework.security.oauth2.core.OAuth2ErrorCodes.INVALID_REQUEST
import org.springframework.security.oauth2.core.OAuth2TokenValidator
import org.springframework.security.oauth2.core.OAuth2TokenValidatorResult
import org.springframework.security.oauth2.core.OAuth2TokenValidatorResult.failure
import org.springframework.security.oauth2.core.OAuth2TokenValidatorResult.success
import org.springframework.security.oauth2.jwt.Jwt
import org.springframework.stereotype.Component

@Component
internal class JwtIssuerUriValidator(
    private val resourceServerProperties: MultiTenantOauth2ResourceServerProperties,
) : OAuth2TokenValidator<Jwt> {

    override fun validate(token: Jwt): OAuth2TokenValidatorResult {
        return when {
            token.issuer == null || isIssuerUnknown(token) -> failure(createIssuerNotKnownError(token))
            else -> success()
        }
    }

    private fun createIssuerNotKnownError(token: Jwt): OAuth2Error = OAuth2Error(
        INVALID_REQUEST,
        "JWT issuer: [${token.issuer}] is unknown",
        "https://tools.ietf.org/html/rfc6750#section-3.1")

    private fun isIssuerUnknown(token: Jwt) = resourceServerProperties
        .map(OAuth2ResourceServerProperties.Jwt::getIssuerUri)
        .none { token.issuer.toString().equals(it, ignoreCase = true) }
}
