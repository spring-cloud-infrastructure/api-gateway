package com.spring.cloud.oauth2.resource.server.jwt.exchange

import com.spring.cloud.config.oauth2.resource.server.jwt.exchange.TokenExchangeProperties
import com.spring.cloud.oauth2.resource.server.jwt.exchange.validator.CachedTokenValidator
import com.spring.cloud.oauth2.resource.server.jwt.model.ResourceAccessJwt
import mu.KotlinLogging.logger
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.cache.Cache.ValueWrapper
import org.springframework.cache.CacheManager
import org.springframework.stereotype.Service
import reactor.cache.CacheMono
import reactor.core.publisher.Mono
import reactor.core.publisher.Signal
import reactor.kotlin.core.publisher.toMono

@Service
internal class CachedTokenExchangeService(
    private val tokenExchangeProperties: TokenExchangeProperties,
    private val cacheManager: CacheManager,
    @Qualifier("defaultTokenExchangeService") private val defaultTokenExchangeService: TokenExchangeService,
    private val cachedTokenValidator: CachedTokenValidator,
) : TokenExchangeService {

    private val logger = logger { }

    override fun exchangeAccessToken(token: ResourceAccessJwt): Mono<ResourceAccessJwt> {
        return CacheMono.lookup(::getTokenExchangeCache, token.subject)
            .onCacheMissResume { defaultTokenExchangeService.exchangeAccessToken(token) }
            .andWriteWith(::setTokenExchangeCache)
    }

    private fun getTokenExchangeCache(tokenSubject: String): Mono<Signal<out ResourceAccessJwt>> {
        return getCache()
            ?.let { it[tokenSubject] }
            ?.let(ValueWrapper::get)
            ?.takeIf { it is ResourceAccessJwt }
            ?.let { it as ResourceAccessJwt }
            ?.takeIf(::isCachedJwtValid)
            ?.let { Signal.next(it) }
            ?.toMono()
            ?: Mono.empty()
    }

    private fun isCachedJwtValid(token: ResourceAccessJwt): Boolean {
        val validatorResult = cachedTokenValidator.validate(token)
        return when {
            validatorResult.hasErrors() -> {
                logger.debug { "Invalid JWT token found in [${tokenExchangeProperties.cache.name}] cache, errors: ${validatorResult.errors}" }
                false
            }

            else -> {
                logger.debug { "Valid JWT token found in [${tokenExchangeProperties.cache.name}] cache, token expiration time: [${token.expiresAt}]" }
                true
            }
        }
    }

    private fun setTokenExchangeCache(
        tokenSubject: String,
        exchangedJwtSignal: Signal<out ResourceAccessJwt>,
    ): Mono<Void> = Mono.fromRunnable {
        exchangedJwtSignal.get()?.apply {
            getCache()
                ?.also { logger.info { "Storing exchanged token for subject: [$tokenSubject] in cache, token expiration time: [${this.expiresAt}]" } }
                ?.put(tokenSubject, this)
        }
    }

    private fun getCache() = cacheManager.getCache(tokenExchangeProperties.cache.name)
}
