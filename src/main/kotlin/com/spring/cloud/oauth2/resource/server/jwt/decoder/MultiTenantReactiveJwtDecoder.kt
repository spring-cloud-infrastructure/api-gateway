package com.spring.cloud.oauth2.resource.server.jwt.decoder

import com.nimbusds.jwt.JWTParser
import com.spring.cloud.config.oauth2.resource.server.jwt.decoder.MultiTenantOauth2ResourceServerProperties
import com.spring.cloud.problem.NoSuitableDecoderFoundProblem
import com.spring.cloud.problem.UnparseableTokenProblem
import mu.KotlinLogging.logger
import org.springframework.security.oauth2.jwt.Jwt
import org.springframework.security.oauth2.jwt.JwtTimestampValidator
import org.springframework.security.oauth2.jwt.NimbusReactiveJwtDecoder
import org.springframework.security.oauth2.jwt.ReactiveJwtDecoder
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.client.WebClient
import reactor.core.publisher.Mono
import reactor.kotlin.core.publisher.toMono
import java.text.ParseException
import java.time.Clock

@Component
internal class MultiTenantReactiveJwtDecoder(
    clock: Clock,
    resourceServerProperties: MultiTenantOauth2ResourceServerProperties,
    webClient: WebClient,
) : ReactiveJwtDecoder {

    private val logger = logger { }

    private val jwtDecodersByIssuerUri: MutableMap<String, ReactiveJwtDecoder> = HashMap()

    init {
        resourceServerProperties.forEach {
            val jwtDecoder = NimbusReactiveJwtDecoder
                .withJwkSetUri(it.jwkSetUri)
                .webClient(webClient)
                .build().apply {
                    setJwtValidator(JwtTimestampValidator().apply {
                        setClock(clock)
                    })
                }
            jwtDecodersByIssuerUri[it.issuerUri] = jwtDecoder
        }
    }

    override fun decode(token: String): Mono<Jwt> = token.toMono()
        .map(::getTokenIssuerUri)
        .map(::getJwtDecoder)
        .flatMap { it.decode(token) }

    private fun getTokenIssuerUri(token: String): String = try {
        JWTParser.parse(token).jwtClaimsSet.issuer

    } catch (e: ParseException) {
        logger.error(e) { "Token issuer URI cannot be extracted" }
        throw UnparseableTokenProblem()
    }

    private fun getJwtDecoder(issuerUri: String): ReactiveJwtDecoder {
        if (jwtDecodersByIssuerUri.containsKey(issuerUri)) {
            return jwtDecodersByIssuerUri[issuerUri]!!
        }

        throw NoSuitableDecoderFoundProblem(issuerUri)
    }
}
