package com.spring.cloud.oauth2.resource.server.jwt.provider

import com.spring.cloud.config.oauth2.resource.server.jwt.provider.AuthorizationServerApiClientConfiguration
import com.spring.cloud.oauth2.resource.server.jwt.provider.model.TokenExchangeRequest
import net.minidev.json.JSONObject
import org.springframework.http.HttpHeaders.AUTHORIZATION
import org.springframework.http.MediaType.APPLICATION_FORM_URLENCODED_VALUE
import org.springframework.http.MediaType.APPLICATION_JSON_VALUE
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestHeader
import reactivefeign.spring.config.ReactiveFeignClient
import reactor.core.publisher.Mono

@ReactiveFeignClient(
    name = "authorization-server",
    configuration = [AuthorizationServerApiClientConfiguration::class]
)
interface AuthorizationServerApiClient {

    @PostMapping(
        path = ["/auth/realms/{realmName}/protocol/openid-connect/token"],
        consumes = [APPLICATION_FORM_URLENCODED_VALUE],
        produces = [APPLICATION_JSON_VALUE]
    )
    fun exchangeToken(
        @RequestHeader(AUTHORIZATION) authorizationHeader: String,
        @PathVariable("realmName") realmName: String,
        tokenExchangeRequest: TokenExchangeRequest,
    ): Mono<JSONObject>
}
