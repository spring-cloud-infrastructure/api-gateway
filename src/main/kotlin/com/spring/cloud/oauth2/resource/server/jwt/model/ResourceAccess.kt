package com.spring.cloud.oauth2.resource.server.jwt.model

data class ResourceAccess(
    var resourceName: String,
    val roles: Set<String>,
)
