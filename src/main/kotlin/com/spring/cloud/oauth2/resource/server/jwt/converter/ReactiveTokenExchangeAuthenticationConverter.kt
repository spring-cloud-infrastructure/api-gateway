package com.spring.cloud.oauth2.resource.server.jwt.converter

import com.spring.cloud.oauth2.resource.server.jwt.exchange.TokenExchangeService
import com.spring.cloud.oauth2.resource.server.jwt.exchange.filter.TokenExchangeApplicableFilter
import com.spring.cloud.oauth2.resource.server.jwt.model.ResourceAccessJwt
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.core.convert.converter.Converter
import org.springframework.security.oauth2.jwt.Jwt
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken
import org.springframework.stereotype.Component
import reactor.core.publisher.Mono
import reactor.kotlin.core.publisher.toMono

/**
 * Converts [Jwt] into [JwtAuthenticationToken], while performs a token exchange in case the input JWT is
 * suitable for token exchange
 *
 * @see TokenExchangeApplicableFilter
 */
@Component
class ReactiveTokenExchangeAuthenticationConverter(
    private val tokenExchangeApplicableFilter: TokenExchangeApplicableFilter,
    @Qualifier("cachedTokenExchangeService") private val cachedTokenExchangeService: TokenExchangeService,
    private val jwtGrantedAuthorityConverter: ReactiveJwtGrantedAuthorityConverter,
) : Converter<Jwt, Mono<JwtAuthenticationToken>> {

    override fun convert(token: Jwt): Mono<JwtAuthenticationToken> = token.toMono()
        .map(::ResourceAccessJwt)
        .flatMap(::exchangeTokenIfApplicable)
        .flatMap(::mapToJwtAuthenticationToken)

    private fun exchangeTokenIfApplicable(token: ResourceAccessJwt): Mono<ResourceAccessJwt> = token.toMono()
        .filter(tokenExchangeApplicableFilter)
        .flatMap(cachedTokenExchangeService::exchangeAccessToken)
        .defaultIfEmpty(token)

    private fun mapToJwtAuthenticationToken(token: ResourceAccessJwt): Mono<JwtAuthenticationToken> = token.toMono()
        .flatMap(jwtGrantedAuthorityConverter::convert)
        .map { JwtAuthenticationToken(token, it) }
}
