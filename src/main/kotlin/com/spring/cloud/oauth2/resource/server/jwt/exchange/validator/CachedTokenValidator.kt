package com.spring.cloud.oauth2.resource.server.jwt.exchange.validator

import com.spring.cloud.config.oauth2.resource.server.jwt.exchange.TokenExchangeProperties
import org.springframework.security.oauth2.core.OAuth2TokenValidator
import org.springframework.security.oauth2.core.OAuth2TokenValidatorResult
import org.springframework.security.oauth2.jwt.Jwt
import org.springframework.stereotype.Component
import java.time.Clock

@Component
class CachedTokenValidator(
    clock: Clock,
    tokenExchangeProperties: TokenExchangeProperties,
) : OAuth2TokenValidator<Jwt> {

    private val jwtValidator: OAuth2TokenValidator<Jwt> = JwtExpirationTimeValidator(
        clock,
        tokenExchangeProperties.cache.minimalJwtExpirationTime
    )

    override fun validate(token: Jwt): OAuth2TokenValidatorResult {
        return jwtValidator.validate(token)
    }
}
