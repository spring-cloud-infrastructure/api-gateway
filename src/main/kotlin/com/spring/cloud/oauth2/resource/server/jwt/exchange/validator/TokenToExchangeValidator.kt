package com.spring.cloud.oauth2.resource.server.jwt.exchange.validator

import com.spring.cloud.config.oauth2.resource.server.jwt.exchange.TokenExchangeProperties
import org.apache.commons.lang3.StringUtils.isNoneBlank
import org.springframework.security.oauth2.core.DelegatingOAuth2TokenValidator
import org.springframework.security.oauth2.core.OAuth2TokenValidator
import org.springframework.security.oauth2.core.OAuth2TokenValidatorResult
import org.springframework.security.oauth2.jwt.Jwt
import org.springframework.security.oauth2.jwt.JwtClaimValidator
import org.springframework.stereotype.Component
import java.time.Clock
import java.util.function.Predicate

@Component
internal class TokenToExchangeValidator(
    clock: Clock,
    tokenExchangeProperties: TokenExchangeProperties,
    jwtIssuerUriValidator: JwtIssuerUriValidator,
) : OAuth2TokenValidator<Jwt> {


    private val jwtValidators = DelegatingOAuth2TokenValidator(
        tokenExchangeProperties.requiredClaims
            .map {
                val predicate = Predicate<String> { claimValue -> isNoneBlank(claimValue) }
                JwtClaimValidator(it, predicate)
            }.toMutableList<OAuth2TokenValidator<Jwt>>().apply {
                add(jwtIssuerUriValidator)
                add(JwtExpirationTimeValidator(clock, tokenExchangeProperties.minimalJwtExpirationTime))
            }
    )

    override fun validate(token: Jwt): OAuth2TokenValidatorResult {
        return jwtValidators.validate(token)
    }
}
