package com.spring.cloud.oauth2.resource.server.jwt.exchange

import com.spring.cloud.oauth2.resource.server.jwt.model.ResourceAccessJwt
import reactor.core.publisher.Mono

interface TokenExchangeService {

    fun exchangeAccessToken(token: ResourceAccessJwt): Mono<ResourceAccessJwt>
}
