package com.spring.cloud.config

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding
import java.io.File
import javax.validation.constraints.NotBlank

@ConstructorBinding
@ConfigurationProperties("javax.net.ssl")
data class SslProperties(
    val keyStore: File,
    val keyStoreType: KeyStoreType,
    @NotBlank val keyStorePassword: String,
    val trustStore: File,
    val trustStoreType: KeyStoreType,
    @NotBlank val trustStorePassword: String,
) {

    enum class KeyStoreType {

        PKCS12,
        JKS
    }
}
