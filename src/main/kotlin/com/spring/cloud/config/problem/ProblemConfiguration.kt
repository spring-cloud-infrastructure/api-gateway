package com.spring.cloud.config.problem

import com.fasterxml.jackson.databind.ObjectMapper
import com.spring.cloud.problem.handler.ProblemHandler
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.core.annotation.Order
import org.springframework.web.server.WebExceptionHandler
import org.zalando.problem.ProblemModule
import org.zalando.problem.spring.webflux.advice.ProblemExceptionHandler
import org.zalando.problem.violations.ConstraintViolationProblemModule


@Configuration
internal class ProblemConfiguration {

    @Bean
    fun problemModule(): ProblemModule = ProblemModule()

    @Bean
    fun constraintViolationProblemModule(): ConstraintViolationProblemModule = ConstraintViolationProblemModule()

    @Bean
    @Order(-2)
    fun problemExceptionHandler(mapper: ObjectMapper, problemHandler: ProblemHandler): WebExceptionHandler =
        ProblemExceptionHandler(mapper, problemHandler)
}
