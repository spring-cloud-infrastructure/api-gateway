package com.spring.cloud.config

import com.spring.cloud.config.oauth2.client.provider.KeycloakProviderProperties
import com.spring.cloud.config.oauth2.client.registration.KeycloakRegistrationProperties
import com.spring.cloud.oauth2.resource.server.jwt.converter.ReactiveTokenExchangeAuthenticationConverter
import io.netty.handler.ssl.ClientAuth.REQUIRE
import io.netty.handler.ssl.SslContextBuilder
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Import
import org.springframework.http.client.reactive.ReactorClientHttpConnector
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity
import org.springframework.security.config.web.server.ServerHttpSecurity
import org.springframework.security.oauth2.client.oidc.web.server.logout.OidcClientInitiatedServerLogoutSuccessHandler
import org.springframework.security.oauth2.client.registration.ClientRegistration
import org.springframework.security.oauth2.client.registration.InMemoryReactiveClientRegistrationRepository
import org.springframework.security.oauth2.client.registration.ReactiveClientRegistrationRepository
import org.springframework.security.oauth2.core.AuthenticationMethod
import org.springframework.security.oauth2.core.AuthorizationGrantType
import org.springframework.security.oauth2.core.ClientAuthenticationMethod
import org.springframework.security.web.server.SecurityWebFilterChain
import org.springframework.security.web.server.authentication.logout.ServerLogoutSuccessHandler
import org.springframework.security.web.server.header.XFrameOptionsServerHttpHeadersWriter.Mode.SAMEORIGIN
import org.springframework.web.reactive.function.client.WebClient
import org.zalando.problem.spring.webflux.advice.security.SecurityProblemSupport
import reactor.netty.http.client.HttpClient
import java.io.File
import java.io.FileInputStream
import java.security.KeyStore
import javax.net.ssl.KeyManagerFactory
import javax.net.ssl.TrustManagerFactory

/**
 * @see [Spring Security Reactive Application Reference](https://docs.spring.io/spring-security/site/docs/current/reference/html5/.reactive-applications)
 */
@EnableWebFluxSecurity
@Import(SecurityProblemSupport::class)
internal class WebConfiguration {

    companion object {
        private const val POST_LOGOUT_REDIRECT_URI = "{baseUrl}"
    }

    @Bean
    fun springSecurityFilterChain(
        http: ServerHttpSecurity,
        tokenExchangeAuthenticationConverter: ReactiveTokenExchangeAuthenticationConverter,
        oidcLogoutSuccessHandler: ServerLogoutSuccessHandler,
        securityProblemSupport: SecurityProblemSupport,
    ): SecurityWebFilterChain {

        // TODO: DSL is not available in this version of Spring yet (5.3.8), but since 5.4
        return http.apply {
            oauth2ResourceServer().apply {
                jwt().apply {
                    jwtAuthenticationConverter(tokenExchangeAuthenticationConverter)
                }
            }

            oauth2Login()

            oauth2Client()

            logout().apply {
                logoutSuccessHandler(oidcLogoutSuccessHandler)
            }

            authorizeExchange().apply {
                // Config Server
                pathMatchers("/config-server/encrypt").apply {
                    hasAuthority("SCOPE_CONFIG-SERVER:ENCRYPT")
                }
                pathMatchers("/config-server/decrypt").apply {
                    hasAuthority("SCOPE_CONFIG-SERVER:DECRYPT")
                }
                pathMatchers("/config-server/actuator/bus-refresh").apply {
                    hasAuthority("SCOPE_CONFIG-SERVER:REFRESH_CONTEXT")
                }

                // Spring Boot Admin
                pathMatchers("/actuator/**", "/auth/**").apply {
                    permitAll()
                }
                pathMatchers("/spring-boot-admin/**").apply {
                    hasAuthority("SCOPE_SPRING-BOOT-ADMIN:SPRING_BOOT_ADMIN")
                }
                pathMatchers("/kibana/**").apply {
                    hasAuthority("SCOPE_SPRING-BOOT-ADMIN:KIBANA")
                }
                pathMatchers("/zipkin/**").apply {
                    hasAuthority("SCOPE_SPRING-BOOT-ADMIN:ZIPKIN")
                }
                pathMatchers("/rabbit-mq/**").apply {
                    hasAuthority("SCOPE_SPRING-BOOT-ADMIN:RABBIT_MQ")
                }

                anyExchange().apply {
                    authenticated()
                }
            }

            headers().apply {
                frameOptions().apply {
                    // Allow showing pages within a frame
                    mode(SAMEORIGIN)
                }
            }

            csrf().apply {
                disable()
            }

            redirectToHttps()

            exceptionHandling().apply {
                authenticationEntryPoint(securityProblemSupport)
                accessDeniedHandler(securityProblemSupport)
            }
        }.build()
    }

    @Bean
    fun clientRegistrationRepository(clientRegistration: ClientRegistration): ReactiveClientRegistrationRepository =
        InMemoryReactiveClientRegistrationRepository(clientRegistration)

    /**
     * We have to create [ClientRegistration] bean manually because we shall provide the `end_session_endpoint` uri as
     * part of provider configuration metadata to enable Open ID Log Out functionality.
     *
     *
     * `end_session_endpoint` might be set also automatically by providing `issuer-uri` in properties, but it
     * requires the authorization-server running up when api-gateway starts. The mentioned option would make us unable
     * to start api-gateway as well as authorization-server independently + we couldn't use api-gateway as a proxy for
     * authorization-server as issuer-uri should directly point to authorization-server.
     */
    @Bean
    fun keycloakClientRegistration(
        registrationProperties: KeycloakRegistrationProperties,
        providerProperties: KeycloakProviderProperties,
    ): ClientRegistration {

        val clientAuthenticationMethod = ClientAuthenticationMethod(registrationProperties.clientAuthenticationMethod)
        val authorizationGrantType = AuthorizationGrantType(registrationProperties.authorizationGrantType)
        val userInfoAuthenticationMethod = AuthenticationMethod(providerProperties.userInfoAuthenticationMethod)
        return ClientRegistration.withRegistrationId(registrationProperties.clientName).apply {
            clientId(registrationProperties.clientId)
            clientSecret(registrationProperties.clientSecret)
            clientAuthenticationMethod(clientAuthenticationMethod)
            authorizationGrantType(authorizationGrantType)
            redirectUriTemplate(registrationProperties.redirectUri)
            scope(registrationProperties.scope)
            authorizationUri(providerProperties.authorizationUri)
            tokenUri(providerProperties.tokenUri)
            userInfoUri(providerProperties.userInfoUri)
            userInfoAuthenticationMethod(userInfoAuthenticationMethod)
            userNameAttributeName(providerProperties.userNameAttribute)
            jwkSetUri(providerProperties.jwkSetUri)
            providerConfigurationMetadata(providerProperties.configurationMetadata)
            clientName(registrationProperties.clientName)
        }.build()
    }

    @Bean
    fun oidcLogoutSuccessHandler(clientRegistrationRepository: ReactiveClientRegistrationRepository): ServerLogoutSuccessHandler =
        OidcClientInitiatedServerLogoutSuccessHandler(clientRegistrationRepository).apply {
            // we shall set uri because otherwise we would not get redirected to login URL after logout
            setPostLogoutRedirectUri(POST_LOGOUT_REDIRECT_URI)
        }

    @Bean
    fun HttpClient(sslProperties: SslProperties): HttpClient = HttpClient.create().secure {
        val sslContext = SslContextBuilder.forClient()
            .clientAuth(REQUIRE)
            .keyManager(buildKeyManagerFactory(sslProperties))
            .trustManager(buildTrustManagerFactory(sslProperties))
            .build()
        it.sslContext(sslContext)
    }

    @Bean
    fun webClient(httpClient: HttpClient): WebClient = WebClient.builder()
        .clientConnector(ReactorClientHttpConnector(httpClient))
        .build()

    private fun buildKeyManagerFactory(sslProperties: SslProperties): KeyManagerFactory =
        KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm()).apply {
            val keyStore = buildKeyStore(
                sslProperties.keyStoreType,
                sslProperties.keyStore,
                sslProperties.keyStorePassword
            )

            init(keyStore, sslProperties.keyStorePassword.toCharArray())
        }

    private fun buildTrustManagerFactory(sslProperties: SslProperties): TrustManagerFactory =
        TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm()).apply {
            val trustStore = buildKeyStore(
                sslProperties.trustStoreType,
                sslProperties.trustStore,
                sslProperties.trustStorePassword
            )

            init(trustStore)
        }

    private fun buildKeyStore(
        keyStoreType: SslProperties.KeyStoreType,
        keyStoreFile: File,
        keyStorePassword: String,
    ): KeyStore = KeyStore.getInstance(keyStoreType.name).apply {
        FileInputStream(keyStoreFile).use { load(it, keyStorePassword.toCharArray()) }
    }
}
