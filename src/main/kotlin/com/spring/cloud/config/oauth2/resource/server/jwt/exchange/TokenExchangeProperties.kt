package com.spring.cloud.config.oauth2.resource.server.jwt.exchange

import org.hibernate.validator.constraints.time.DurationMin
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding
import java.time.Duration
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotEmpty

@ConstructorBinding
@ConfigurationProperties("services.api-gateway.token-exchange")
class TokenExchangeProperties(
    @NotEmpty val applicableClients: MutableSet<String>,
    @DurationMin(minutes = 1) val minimalJwtExpirationTime: Duration,
    @NotEmpty val requiredClaims: MutableSet<String>,
    val cache: TokenExchangeCacheProperties,
) {

    data class TokenExchangeCacheProperties(
        @NotBlank val name: String,
        @DurationMin(minutes = 1) val timeToLive: Duration,
        @DurationMin(minutes = 1) val minimalJwtExpirationTime: Duration,
    )
}
