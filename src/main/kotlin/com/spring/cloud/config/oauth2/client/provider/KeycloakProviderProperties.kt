package com.spring.cloud.config.oauth2.client.provider

import org.hibernate.validator.constraints.URL
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotEmpty

@ConstructorBinding
@ConfigurationProperties("spring.security.oauth2.client.provider.keycloak")
data class KeycloakProviderProperties(
    @URL val authorizationUri: String,
    @URL val tokenUri: String,
    @URL val userInfoUri: String,
    @URL val jwkSetUri: String,
    @NotBlank val userNameAttribute: String,
    @NotBlank val userInfoAuthenticationMethod: String,
    @NotEmpty val configurationMetadata: Map<String, String>,
)
