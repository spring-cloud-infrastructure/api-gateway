package com.spring.cloud.config.oauth2.resource.server.jwt.decoder

import org.springframework.boot.autoconfigure.security.oauth2.resource.OAuth2ResourceServerProperties.Jwt
import org.springframework.boot.context.properties.ConfigurationProperties
import java.util.HashSet

@ConfigurationProperties("spring.security.oauth2.resourceserver.multi-tenant-jwt")
class MultiTenantOauth2ResourceServerProperties : HashSet<Jwt>()
