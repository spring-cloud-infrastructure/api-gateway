package com.spring.cloud.config.oauth2.resource.server.jwt.provider

import com.spring.cloud.oauth2.resource.server.jwt.provider.AuthorizationServerApiClient
import feign.codec.Encoder
import feign.form.spring.SpringFormEncoder
import mu.KotlinLogging.logger
import org.springframework.beans.factory.ObjectFactory
import org.springframework.boot.autoconfigure.http.HttpMessageConverters
import org.springframework.cloud.openfeign.support.SpringEncoder
import org.springframework.context.annotation.Bean
import reactivefeign.client.log.DefaultReactiveLogger
import java.time.Clock

class AuthorizationServerApiClientConfiguration {

    @Bean
    fun feignEncoder(messageConverters: ObjectFactory<HttpMessageConverters>): Encoder =
        SpringFormEncoder(SpringEncoder(messageConverters))

    @Bean
    fun feignLogger(clock: Clock): DefaultReactiveLogger {
        val authorizationServerApiClientLogger = logger(AuthorizationServerApiClient::class.java.name)
        return DefaultReactiveLogger(clock, authorizationServerApiClientLogger)
    }
}
