package com.spring.cloud.config.oauth2.client.registration

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotEmpty

@ConstructorBinding
@ConfigurationProperties("spring.security.oauth2.client.registration.keycloak")
class KeycloakRegistrationProperties(
    @NotBlank val clientId: String,
    @NotBlank val clientSecret: String,
    @NotBlank val clientName: String,
    @NotBlank val authorizationGrantType: String,
    @NotBlank val redirectUri: String,
    @NotBlank val clientAuthenticationMethod: String,
    @NotEmpty val scope: Set<String>,
)
