package com.spring.cloud.config.cache

import com.hazelcast.config.Config
import com.hazelcast.config.MapConfig
import com.spring.cloud.config.oauth2.resource.server.jwt.exchange.TokenExchangeProperties
import org.springframework.cache.annotation.EnableCaching
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
@EnableCaching
internal class HazelcastCacheConfiguration {

    @Bean
    fun hazelcastCacheConfig(tokenExchangeProperties: TokenExchangeProperties): Config = Config().apply {
        val tokenExchangeCache = tokenExchangeProperties.cache
        this.mapConfigs[tokenExchangeCache.name] = MapConfig().apply {
            name = tokenExchangeCache.name
            timeToLiveSeconds = tokenExchangeCache.timeToLive.toSeconds().toInt()
        }
    }
}
