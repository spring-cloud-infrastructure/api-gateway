import org.gradle.api.tasks.testing.logging.TestLogEvent.FAILED
import org.gradle.api.tasks.testing.logging.TestLogEvent.PASSED
import org.gradle.api.tasks.testing.logging.TestLogEvent.SKIPPED
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import org.springframework.boot.gradle.tasks.bundling.BootBuildImage
import org.springframework.boot.gradle.tasks.bundling.BootJar

plugins {
    val springBootPluginVersion = "2.3.9.RELEASE"
    val springDependencyManagementPluginVersion = "1.0.11.RELEASE"
    val buildInfoPluginVersion = "0.1.3"
    val kotlinPluginVersion = "1.4.31"

    id("org.springframework.boot") version springBootPluginVersion
    id("io.spring.dependency-management") version springDependencyManagementPluginVersion
    id("com.pasam.gradle.buildinfo") version buildInfoPluginVersion
    kotlin("jvm") version kotlinPluginVersion
    kotlin("plugin.spring") version kotlinPluginVersion
    kotlin("kapt") version kotlinPluginVersion
}

group = "com.spring.cloud"
version = "1.0.0-SNAPSHOT"
val javaVersion = JavaVersion.VERSION_15

repositories {
    mavenLocal()
    mavenCentral()
    maven {
        name = "GitLab"
        url = uri("https://gitlab.com/api/v4/groups/9994989/-/packages/maven")
    }
}

dependencies {
    val springCloudStarterVersionAwareServiceDiscoveryClientVersion = "1.0.0-SNAPSHOT"
    val logstashLogbackEncoderVersion = "6.6"
    val kotlinLoggingJvmVersion = "2.0.6"
    val problemSpringWebfluxVersion = "0.26.2"
    implementation(kotlin("stdlib-jdk8"))
    implementation(kotlin("reflect"))
    implementation("com.spring.cloud:spring-cloud-starter-version-aware-service-discovery-client:$springCloudStarterVersionAwareServiceDiscoveryClientVersion")
    implementation("org.springframework.cloud:spring-cloud-starter-config")
    implementation("org.springframework.boot:spring-boot-starter-actuator")
    implementation("org.springframework.retry:spring-retry")
    implementation("org.springframework:spring-aspects")
    implementation("org.springframework.cloud:spring-cloud-starter-gateway")
    implementation("org.springframework.cloud:spring-cloud-starter-zipkin")
    implementation("org.springframework.boot:spring-boot-starter-amqp")
    implementation("net.logstash.logback:logstash-logback-encoder:$logstashLogbackEncoderVersion")
    implementation("io.github.microutils:kotlin-logging-jvm:$kotlinLoggingJvmVersion")
    implementation("com.playtika.reactivefeign:feign-reactor-cloud2")
    implementation("com.playtika.reactivefeign:feign-reactor-spring-configuration")
    implementation("com.playtika.reactivefeign:feign-reactor-webclient")
    implementation("org.springframework.boot:spring-boot-starter-security")
    implementation("org.springframework.boot:spring-boot-starter-oauth2-client")
    implementation("org.springframework.boot:spring-boot-starter-oauth2-resource-server")
    implementation("org.springframework.boot:spring-boot-starter-cache")
    implementation("com.hazelcast:hazelcast-spring")
    implementation("org.apache.commons:commons-lang3")
    implementation("io.projectreactor.kotlin:reactor-kotlin-extensions")
    implementation("org.zalando:problem-spring-webflux:$problemSpringWebfluxVersion")

    kapt("org.springframework.boot:spring-boot-configuration-processor")

    // 3.x.x ain't working due to incompatibility with Spring 2.3.x, it requires 2.4.x instead
    val springMockkVersion = "2.0.3"
    val assertkVersion = "0.23.1"
    val kotlinWiremockVersion = "1.0.0"
    testImplementation("com.ninja-squad:springmockk:$springMockkVersion")
    testImplementation("com.willowtreeapps.assertk:assertk:$assertkVersion")
    testImplementation("com.marcinziolo:kotlin-wiremock:$kotlinWiremockVersion")
    testImplementation("org.springframework.cloud:spring-cloud-starter-contract-stub-runner")
    testImplementation("org.springframework.security:spring-security-test")
    testImplementation("org.springframework.amqp:spring-rabbit-test")
    testImplementation("org.springframework.boot:spring-boot-starter-test") {
        exclude(module = "mockito-core")
        exclude(module = "mockito-junit-jupiter")
    }
}

dependencyManagement {
    val springCloudDependenciesBomVersion = "Hoxton.SR10"
    val feignReactorBomVersion = "2.0.29"
    imports {
        mavenBom("org.springframework.cloud:spring-cloud-dependencies:$springCloudDependenciesBomVersion")
        mavenBom("com.playtika.reactivefeign:feign-reactor-bom:$feignReactorBomVersion")
    }
}

configure<JavaPluginConvention> {
    sourceCompatibility = javaVersion
    targetCompatibility = javaVersion
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        freeCompilerArgs = listOf("-Xjsr305=strict")
        jvmTarget = javaVersion.toString()
    }
}

tasks.getByName<BootJar>("bootJar") {
    mainClassName = "com.spring.cloud.ApiGatewayApplicationKt"
}

tasks.getByName<BootBuildImage>("bootBuildImage") {
    imageName = "${project.group}/${project.name}"
    environment = mapOf(
        "BP_DEBUG_ENABLED" to "true"
    )
}

tasks.withType<Test> {
    useJUnitPlatform()
    testLogging {
        showExceptions = true
        showStandardStreams = true
        events(PASSED, SKIPPED, FAILED)
    }
}
