# API Gateway

Check out [documentation](https://cloud.spring.io/spring-cloud-gateway/reference/html/) for more info about Spring Cloud
Gateway.

## 1. Features

- [Reactive Feign](https://github.com/Playtika/feign-reactive/tree/develop/feign-reactor-spring-configuration) with
  `Spring WebFlux`, `Spring Cloud Load Balancer` and `Spring Cloud Circuit Breaker`.

## 2. Configuration

- [bootstrap.yml](./src/main/resources/bootstrap.yml) (application bootstrap only)
- [config-repo](https://gitlab.com/spring-cloud-infrastructure/config-repo/-/tree/master/config) (externalized
  configuration)

## 3. Build & Run

Check out [docker](https://gitlab.com/spring-cloud-infrastructure/docker) project.
